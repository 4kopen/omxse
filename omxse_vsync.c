/************************************************************************
Copyright (C) 2005 STMicroelectronics. All Rights Reserved.

This file is part of the Dvb_adaptation Library.

Dvb_adaptation is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Dvb_adaptation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Dvb_adaptation Library may alternatively be licensed under a proprietary
license from ST.

Source file name : omxse_vsync.c
Author :

************************************************************************/

#include <linux/module.h>            /* kernel module definitions */
#include <linux/fs.h>                /* file system operations */
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/time.h>
#include <linux/moduleparam.h>

#include <stm_display.h>
#include <linux/stm/stmcoredisplay.h>

#include "omxse.h"
#include "omxse_internal.h"


//Number of vsync managable by this module
//Let's start just with Main display, tbc.
#define NR_DISPLAY_VSYNC 1
static struct stmcore_display_pipeline_data platform_data[NR_DISPLAY_VSYNC];

static struct cdev cdev;
static dev_t omxse_vsync_devno;
struct omxse_vsync_context {
  //Through a IOCTL, the vsync pipeline might be changed
  int                             vsync_pipeline;
  struct mutex                    lock;
};

static struct omxse_vsync_context VSync[NR_DISPLAY_VSYNC];

static int omxse_vsync_open(struct inode *inode, struct file *filp) {
  filp->private_data = (void*)&VSync[0];
  filp->f_pos = 0;
  return 0;
}

static int omxse_vsync_release(struct inode *inode, struct file *filp) {
  return 0;
}

static long omxse_vsync_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
  struct omxse_vsync_context* context = (struct omxse_vsync_context*)filp->private_data;
  // Unused: void __user *argp = (void __user *) arg;
  struct timespec timestamp;
  struct stmcore_display_pipeline_data *pd = (struct stmcore_display_pipeline_data *) &platform_data[context->vsync_pipeline];

  switch(cmd) {
    case OMXSE_WAIT_FOR_VSYNC:

      interruptible_sleep_on(&pd->display_runtime->vsync_wait_queue);

      if(signal_pending(current))
        return -ERESTARTSYS;

      //Still interrupt delay to fix
      ktime_get_ts(&timestamp);

      if (copy_to_user ((void *) arg, &timestamp, sizeof(struct timespec)))
        return -EFAULT;

      break;
  }
  return 0;
}


static struct file_operations fops =
{
  .owner          = THIS_MODULE,
  .open           = omxse_vsync_open,
  .release        = omxse_vsync_release,
  .unlocked_ioctl = omxse_vsync_ioctl,
};

int omxse_vsync_init(dev_t devno, struct class *omxse_class, const char* name)
{
  int result;
  struct device * mdev;
  int nr_platform_devices;
  int i;


  cdev_init(&cdev, &fops);
  result = cdev_add(&cdev, devno, 1);
  if (0 != result) {
    ERROR("cdev_add\n");
    return result;
  }

  mdev = device_create(omxse_class, NULL, devno, NULL, name, MINOR(devno));
  if (IS_ERR(mdev)) {
    ERROR("device_create\n");
    cdev_del(&cdev);
    return PTR_ERR(mdev);
  }

  omxse_vsync_devno = devno;

  nr_platform_devices=0;
  for (i = 0; i < NR_DISPLAY_VSYNC; i++) {
    if(stmcore_get_display_pipeline(i, &platform_data[nr_platform_devices]) != 0)
      continue;
    VSync[nr_platform_devices].vsync_pipeline=i;
    nr_platform_devices++;

  }

  return 0;
}

void omxse_vsync_done(struct class *omxse_class)
{
  device_destroy(omxse_class, omxse_vsync_devno);
  cdev_del(&cdev);
}

