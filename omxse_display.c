/************************************************************************
Copyright (C) 2005 STMicroelectronics. All Rights Reserved.

This file is part of the Dvb_adaptation Library.

Dvb_adaptation is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Dvb_adaptation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Dvb_adaptation Library may alternatively be licensed under a proprietary
license from ST.

Source file name : omxse_display.c
Author :           Jean-Philippe Fassino

************************************************************************/

#include <linux/module.h>            /* kernel module definitions */
#include <linux/fs.h>                /* file system operations */
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/moduleparam.h>


#include "stm_se.h"
#include "osdev_device.h"
#include "omxse.h"
#include "omxse_internal.h"


#define STM_DISPLAY_DEVICE  0

static int stm_display_select[1]= {0};
module_param_array(stm_display_select, int, NULL, 0444);
MODULE_PARM_DESC(stm_display_select, "0:HDMI 1:Analog");

struct omxse_global_display_context {
    int32_t                         openNumber;
    struct mutex                    lock;
    stm_display_device_h            Display;
};

struct omxse_display_context {

    char                            planeName[OMXSE_NAME_LENGTH];
    bool                            displayAllowed;

    stm_rect_t                      inputCrop;
    bool                            inputCropSetted;
    stm_rect_t                      displayWindow;
    bool                            displayWindowSetted;

    // Display
    stm_display_plane_h             mainPlane;
    stm_display_plane_h             auxPlane;
    stm_display_output_h            Output;
    stm_display_source_h            Source;
    stm_display_source_queue_h      QueueInterface;
};

static struct omxse_global_display_context Display;

typedef enum {
    OMXSE_SELECT_ALL,
    OMXSE_SELECT_ANALOG_ONLY
} display_select_t;

struct SEBuffer *last_sebuffer = NULL;

//============================================================================
//  GetDisplayMode
//============================================================================
static display_select_t GetDisplayMode(void)
{
    display_select_t displayMode;

    switch (stm_display_select[0])
    {
    case 0: displayMode = OMXSE_SELECT_ALL; break;
    case 1: displayMode = OMXSE_SELECT_ANALOG_ONLY; break;
    default: displayMode = OMXSE_SELECT_ALL; break;
    }
    return displayMode;
}

extern void omxse_capture_setdisplay(struct SEBuffer* sebuffer, void *display_context);



//============================================================================
//  FinishedCallback
//============================================================================
static void FinishedCallback(void*   Buffer,
        const stm_buffer_presentation_stats_t *Data)
{
    struct SEBuffer* sebuffer = (struct SEBuffer*)Buffer;
    VERBOSE("bufferFinishedCallback(%p : %d)\n", sebuffer, sebuffer->SEUserPrivate);
    omxse_capture_ReleaseSEBufferCallback(sebuffer);
}

//============================================================================
//  displaySEBuffer
//============================================================================
static void displaySEBuffer(struct omxse_display_context* Context, struct SEBuffer* sebuffer)
{
    int Result;

    VERBOSE("OMXSE:: displaySEBuffer(%p : %d)\n", sebuffer, sebuffer->SEUserPrivate);

    sebuffer->DisplayBuffer.info.completed_callback = &FinishedCallback;
    Result = stm_display_source_queue_buffer(Context->QueueInterface, &sebuffer->DisplayBuffer);
    if(Result != 0) {
        ERROR("OMXSE(display): stm_display_source_queue_buffer Failed (%d)\n", Result);
    }
    last_sebuffer = sebuffer;
}

//============================================================================
//  displayDefaultFrame
//============================================================================
static int displayDefaultFrame(struct omxse_display_context* Context, struct SEBuffer* sebuffer)
{
    int Result = -1;

    VERBOSE("displayDefaultFrame(%p : )\n", sebuffer);

    if(Context != NULL)
    {
        if(Context->QueueInterface)
        {
            Result = stm_display_source_queue_buffer(Context->QueueInterface, &sebuffer->DisplayBuffer);
            if(Result != 0)
            {
                ERROR("OMXSE(display): stm_display_source_queue_buffer Failed (%d)\n", Result);
            }
        }
        else
        {
            INFO("QueueInterface is NULL\n");
            Result = -1;
        }
    }
    else
    {
        INFO("Display Context is NULL\n");
    }
    mutex_lock(&Display.lock);
    last_sebuffer = NULL;
    mutex_unlock(&Display.lock);

    return Result;
}

//============================================================================
//  displayClose
//============================================================================
static void displayClose(struct omxse_display_context* Context)
{
    int ret;

    if(Context->mainPlane != NULL)
    {
        INFO("OMXSE(display): Disconnect Plane '%s'\n", Context->planeName);

        // Undo configuration
        ret=stm_display_plane_set_control(Context->mainPlane, PLANE_CTRL_OUTPUT_WINDOW_MODE, AUTO_MODE);
        if (ret) ERROR("OMXSE(display): displayClose:  stm_display_plane_set_control(PLANE_CTRL_OUTPUT_WINDOW_MODE) returned %d\n", ret);
        ret=stm_display_plane_set_control(Context->mainPlane, PLANE_CTRL_INPUT_WINDOW_MODE, AUTO_MODE);
        if (ret) ERROR("OMXSE(display): displayClose:  stm_display_plane_set_control(PLANE_CTRL_INPUT_WINDOW_MODE) returned %d\n", ret);

        if(Context->Output != NULL)
        {
            if(Context->Source != NULL)
            {
                if(Context->QueueInterface != NULL)
                {
                    ret=stm_display_source_queue_unlock(Context->QueueInterface);
                    if (ret) ERROR("OMXSE(display): displayClose:  stm_display_source_queue_unlock() returned %d\n", ret);
                    ret=stm_display_source_queue_release(Context->QueueInterface);
                    if (ret) ERROR("OMXSE(display): displayClose:  stm_display_source_queue_release() returned %d\n", ret);
                    Context->QueueInterface = NULL;
                }

                stm_display_source_close(Context->Source);
                Context->Source = NULL;
            }
            stm_display_output_close(Context->Output);
            Context->Output = NULL;
        }
        stm_display_plane_close(Context->mainPlane);
        Context->mainPlane = NULL;
/*
        if (GetDisplayMode() != OMXSE_SELECT_ANALOG_ONLY) {
          stm_display_plane_close(Context->auxPlane);
          Context->auxPlane = NULL;
        }
*/
    }
}

//============================================================================
//  displayOpen
//============================================================================
static int displayOpen(struct omxse_display_context* Context)
{
    uint32_t planeId, outputId = 0;
    unsigned int SourceId;
    const char *name;
    int Result;
    int ret = 0;

    /* Look for main plane only */
    for(planeId=0 ; ; planeId++)
    {
        Result = stm_display_device_open_plane(Display.Display, planeId, &Context->mainPlane);
        if(Result != 0) {
            ERROR("OMXSE(display): Plane '%s' not found\n", Context->planeName);
            goto error;
        }

        if(stm_display_plane_get_name(Context->mainPlane, &name) == 0) {
            if(strcmp(name, Context->planeName) == 0) {
                break;
            }
        }
        stm_display_plane_close(Context->mainPlane);
        Context->mainPlane = NULL;
    }
   INFO("OMXSE displayOpen %s\n",Context->planeName);


    if (GetDisplayMode() != OMXSE_SELECT_ANALOG_ONLY)
    {
        Result = stm_display_device_find_outputs_with_capabilities(Display.Display,
                (stm_display_output_capabilities_t)(OUTPUT_CAPS_PLANE_MIXER | OUTPUT_CAPS_HD_ANALOG | OUTPUT_CAPS_SD_ANALOG),
                (stm_display_output_capabilities_t)(OUTPUT_CAPS_PLANE_MIXER | OUTPUT_CAPS_HD_ANALOG | OUTPUT_CAPS_SD_ANALOG),
                &outputId, 1);
        if (Result <= 0) {
            ERROR("OMXSE(display): OUTPUT_CAPS_PLANE_MIXER | OUTPUT_CAPS_HD_ANALOG | OUTPUT_CAPS_SD_ANALOG) \n");
            ERROR("OMXSE(display): stm_display_device_find_outputs_with_capabilities Failed (%d)\n", Result);
            goto error;
        }
    }

    Result = stm_display_device_open_output(Display.Display, outputId, &Context->Output);
    if (Result != 0) {
        ERROR("OMXSE(display): stm_display_device_open_output Failed (%d)\n", Result);
        goto error;
    }

    if(stm_display_output_get_name(Context->Output, &name) == 0)
      INFO("OMXSE(display): Connect Plane to Output '%s'\n", name);


    Result = stm_display_plane_get_connected_source_id(Context->mainPlane, &SourceId);
    if (Result) {
      ERROR("OMXSE(display): No source  connected to main Plane\n");
      goto error;
    }
    else {
      stm_display_source_interface_params_t InterfaceParams;
      Result = stm_display_device_open_source(Display.Display, SourceId, &Context->Source);
      if (Result != 0) {
        ERROR("OMXSE(display): stm_display_device_open_source Failed (%d) \n", Result);
        goto error;
      }
      ret=stm_display_source_get_name(Context->Source, &name);
      if (ret) ERROR("OMXSE(display): displayOpen:  stm_display_source_get_name() returned %d\n", ret);
      DEBUG("   Source = %d: %s, ret=%d\n", SourceId, name);

      InterfaceParams.interface_type = STM_SOURCE_QUEUE_IFACE;
      Result = stm_display_source_get_interface(Context->Source,
                                                InterfaceParams,
                                                (void **)&Context->QueueInterface);

      DEBUG("   Result get interface = %d: \n", Result);
    }
/*
    if (GetDisplayMode() != OMXSE_SELECT_ANALOG_ONLY) {
      ResultAux = stm_display_plane_get_connected_source_id(Context->auxPlane, &SourceId);

      //So far Just trigger an error msg
      if (ResultAux) {
        ERROR("OMXSE(display): No source  connected to aux Plane\n");
      }
    }
*/
    Result = stm_display_source_queue_lock(Context->QueueInterface);
    if (Result != 0) {
        ERROR("OMXSE(display): stm_display_source_queue_lock Failed (%d)\n", Result);
        goto error;
    }

    if(Context->inputCropSetted)
    {
        if((Result = stm_display_plane_set_compound_control(Context->mainPlane, PLANE_CTRL_INPUT_WINDOW_VALUE, &Context->inputCrop)) != 0)
        {
          ERROR("stm_display_plane_set_compound_control Failed (Input window rect)\n");
          goto error;
        }

        if((Result = stm_display_plane_set_control(Context->mainPlane, PLANE_CTRL_INPUT_WINDOW_MODE, MANUAL_MODE)) != 0)
        {
            ERROR("stm_display_plane_set_control Failed (Input window mode)\n");
            goto error;
        }
    }

    if(Context->displayWindowSetted)
    {
        if((Result = stm_display_plane_set_compound_control(Context->mainPlane, PLANE_CTRL_OUTPUT_WINDOW_VALUE, &Context->displayWindow)) != 0)
        {
          ERROR("stm_display_plane_set_compound_control Failed (Output window rect)\n");
          goto error;
        }

        if((Result = stm_display_plane_set_control(Context->mainPlane, PLANE_CTRL_OUTPUT_WINDOW_MODE, MANUAL_MODE)) != 0)
        {
            ERROR("stm_display_plane_set_control Failed (Output window mode)\n");
            goto error;
        }
    }

    // Set background color to black
    Result = stm_display_output_set_control(Context->Output, OUTPUT_CTRL_BACKGROUND_ARGB, 0x00101010);
    if (Result != 0) {
        ERROR("OMXSE(display): stm_display_output_set_control Failed (%d)\n", Result);
        return Result;
    }

    return 0;
error:
    displayClose(Context);
    Context->displayAllowed = true;
    return Result;
}


//============================================================================
//  omxse_display_open
//     call at module open
//============================================================================
static int omxse_display_open(struct inode *inode, struct file *filp)
{
    struct omxse_display_context *Context;
    int Result = 0;

    Context = kzalloc(sizeof(struct omxse_display_context), GFP_KERNEL);
    if (!Context)
        return -ENOMEM;

    filp->private_data = (void*)Context;
    filp->f_pos = 0;

    mutex_lock(&Display.lock);          //to protect openNumber
    if(Display.openNumber++ < 2)        //Maximum displays allowed is 2
    {
        if(Display.Display == 0)
        {
            /*
             * Open STMFB
             */
            Result = stm_display_open_device(STM_DISPLAY_DEVICE, &Display.Display);
            if(Result != 0)
            {
                ERROR("OMXSE(display): stm_display_open_device Failed (%d)\n", Result);
            }
        }

        if (GetDisplayMode() != OMXSE_SELECT_ANALOG_ONLY)
        {
            strcpy(Context->planeName, "Main-VID");
            INFO("Default OMXSE(display): omxse_display_open: hdmi\n");
        }
        else
        {
            strcpy(Context->planeName, "Aux-VID");
            INFO("Default OMXSE(display): omxse_display_open: analog\n");
        }
        Context->displayAllowed = true;
        Context->inputCropSetted = false;
        Context->displayWindowSetted = false;
    }
    else
    {
        ERROR("OMXSE(display): Maximum display planes already opened\n");
    }
    mutex_unlock(&Display.lock);

    return Result;
}

//============================================================================
//  omxse_display_release
//     call at module close: release the display module
//============================================================================
static int omxse_display_release(struct inode *inode, struct file *filp)
{
    struct omxse_display_context* Context = (struct omxse_display_context*)filp->private_data;

    mutex_lock(&Display.lock);

    if(Display.openNumber > 0)
    {
        displayClose(Context);
        Display.openNumber -= 1;
    }

    if((Display.openNumber == 0) && (Display.Display != 0))
    {
        INFO("OMXSE(display): Closing Display Device : %s: %s\n", __func__, Context->planeName);
        stm_display_device_close(Display.Display);
        Display.Display = 0;
    }

    kfree(Context);
    mutex_unlock(&Display.lock);

    return 0;
}

//============================================================================
//  omxse_display_ioctl
//     management of IOCTLs display module
//============================================================================
static long omxse_display_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    struct omxse_display_context* Context = (struct omxse_display_context*)filp->private_data;
    void __user *argp = (void __user *) arg;
    int ret=0;

    switch(cmd)
    {
    case OMXSE_SET_PLANE: {
        omxse_setplane plane;

        if (copy_from_user(&plane, argp, sizeof(plane)))
            return -EFAULT;

        mutex_lock(&Display.lock);

        if(Context->QueueInterface != NULL)
        {
            ret=stm_display_source_queue_flush (Context->QueueInterface, 0);
            if (ret) ERROR("OMXSE(display): omxse_display_ioctl:  stm_display_source_queue_flush() returned %d\n", ret);
            displayClose(Context);
        }

        //TO FIX, by default only main is change
        strncpy(Context->planeName, plane.planeName, (sizeof(Context->planeName)-1));
        Context->displayAllowed = (strcmp(Context->planeName, "None") != 0);

        mutex_unlock(&Display.lock);
    }; break;

    case OMXSE_SET_PLANE_TRANSPARENCY: {
        uint32_t planeId = 0;
        const char *name = NULL;
        stm_display_plane_h displayPlane = NULL;
        stm_display_plane_h dummyPlane = NULL;
        omxse_display_transparency transparency;

        if (copy_from_user(&transparency, argp, sizeof(transparency)))
            return -EFAULT;

        mutex_lock(&Display.lock);
        if(Context->QueueInterface != NULL)
        {
            /*Check if the display plane is already open*/
            if(Context->mainPlane != NULL) {
                if(stm_display_plane_get_name(Context->mainPlane, &name) == 0) {
                    if(strcmp(name, transparency.planeName) == 0) {
                        INFO("OMXSE(display): %s plane found\n", transparency.planeName);
                        displayPlane = Context->mainPlane;
                    } else
                        displayPlane = NULL;
                } else {
                    ERROR("OMXSE(display): Plane open failed\n");
                    return -EINVAL;
                }
            }
        }
        if (Context->displayAllowed) {
            if (displayPlane == NULL) {
                /* Or Look for plane to Open */
                for(planeId=0 ; ; planeId++)
                {
                    if(stm_display_device_open_plane(Display.Display, planeId, &dummyPlane) != 0) {
                        ERROR("OMXSE(display): Plane '%s' not found\n", transparency.planeName);
                        return -EINVAL;
                    }

                    if(stm_display_plane_get_name(dummyPlane, &name) == 0) {
                        if(strcmp(name, transparency.planeName) == 0) {
                            INFO("OMXSE(display): Plane '%s' found\n", transparency.planeName);
                            displayPlane = dummyPlane;
                            break;
                        }
                    }
                    stm_display_plane_close(dummyPlane);
                }
            }
            /* Set plane Transparency */
            ret=stm_display_plane_set_control(displayPlane, PLANE_CTRL_TRANSPARENCY_STATE, 1);
            if (ret)
                ERROR("OMXSE(display): omxse_display_ioctl:  stm_display_plane_set_control(PLANE_CTRL_TRANSPARENCY_STATE) returned %d\n", ret);

            ret=stm_display_plane_set_control(displayPlane, PLANE_CTRL_TRANSPARENCY_VALUE, transparency.value);
            if (ret)
                ERROR("OMXSE(display): omxse_display_ioctl:  stm_display_plane_set_control(PLANE_CTRL_TRANSPARENCY_VALUE) returned %d\n", ret);

            if (dummyPlane != NULL) {
                stm_display_plane_close(dummyPlane);
            }
        }
        mutex_unlock(&Display.lock);
    }; break;

    case OMXSE_SET_OVERLAY: {
        omxse_display_setoverlay setoverlay;

        if (copy_from_user(&setoverlay, argp, sizeof(setoverlay)))
            return -EFAULT;

        Context->inputCrop.x = setoverlay.inputCropX;
        Context->inputCrop.y = setoverlay.inputCropY;
        Context->inputCrop.width = setoverlay.inputCropW;
        Context->inputCrop.height = setoverlay.inputCropH;

        Context->displayWindow.x = setoverlay.displayX;
        Context->displayWindow.y = setoverlay.displayY;
        Context->displayWindow.width = setoverlay.displayW;
        Context->displayWindow.height = setoverlay.displayH;

        if (Context->QueueInterface != NULL) {
            ret=stm_display_plane_set_compound_control(Context->mainPlane, PLANE_CTRL_OUTPUT_WINDOW_VALUE, &Context->displayWindow);
            if (ret) ERROR("OMXSE(display): omxse_display_ioctl:  stm_display_plane_set_compound_control(PLANE_CTRL_OUTPUT_WINDOW_VALUE) returned %d\n", ret);

            ret=stm_display_plane_set_compound_control(Context->mainPlane, PLANE_CTRL_INPUT_WINDOW_VALUE, &Context->inputCrop);
            if (ret) ERROR("OMXSE(display): omxse_display_ioctl:  stm_display_plane_set_compound_control(PLANE_CTRL_INPUT_WINDOW_VALUE) returned %d\n", ret);
        }

        Context->inputCropSetted = true;
        Context->displayWindowSetted = true;
    }; break;

    case OMXSE_IS_VDP_BUFFER: {
        unsigned long physicalAddr;
        struct OMXBuffer* buffer;


        if(! Context->displayAllowed)
            return EAGAIN;

        physicalAddr = GetPhysicalContiguous(arg, 1);
        if (!physicalAddr)
            return -EIO;

        if((buffer = omxse_buffer_findOMXByPhysAddr(physicalAddr)) != NULL) {
            if(buffer->sebuffer != NULL) {
                omxse_capture_setdisplay(buffer->sebuffer, Context); //set the association btw capture & display
            }
            if (buffer->bltBuffer != NULL) {
                return 0;
            }
        }
        return -EIO;
    }; break;

    case OMXSE_QUEUE_VDP: {
        omxse_queuevdp queuevdp;
        unsigned long physicalAddr;
        struct OMXBuffer* buffer;

        if(! Context->displayAllowed)
            return EAGAIN;

        if (copy_from_user(&queuevdp, argp, sizeof(queuevdp)))
            return -EFAULT;

        DEBUG("OMXSE(display): omxse_file_ioctl (OMXSE_QUEUE_VDP, %lx, %d)\n", queuevdp.virtualAddr, queuevdp.displayRatio);

        physicalAddr = GetPhysicalContiguous(queuevdp.virtualAddr, 1);
        if (!physicalAddr)
            return -EIO;

        buffer = omxse_buffer_findOMXByPhysAddr(physicalAddr);
        if(buffer != NULL)
        {
            if(buffer->sebuffer != NULL)
            {
                struct SEBuffer* sebuffer = buffer->sebuffer;


                mutex_lock(&Display.lock);

                if(Context->QueueInterface == NULL)
                {
                    displayOpen(Context);
                }

                if(Context->QueueInterface != NULL) // Really open ? No error ?
                {
                    if(buffer->sebuffer->isGrThanMaxForResize && (queuevdp.displayRatio <= 100)) {
                        VERBOSE("going to Blit resize\n");
                        omxse_blitter_resizeDisplayBufferInPlace(buffer, MAX_WIDTH_FOR_RESIZE, MAX_HEIGHT_FOR_RESIZE);
                    }
                    // The buffer has been send to Display ; undo association it from OMX buffer
                    buffer->sebuffer = NULL;

                    displaySEBuffer(Context, sebuffer);
                }

                mutex_unlock(&Display.lock);
            }

            return 0;
        }

        ERROR("OMXSE(display): OMXSE_QUEUE_VDP on an buffer not associate with SE Buffer (%lx)\n", physicalAddr);
        return -EIO;
    }; break;

    default:
        ERROR("OMXSE(display): Unknow Ioctl\n");
        return -EINVAL;
    }

    return 0;
}

int omxse_save_display_state(struct SEBuffer *Frame,
                              void (*FrameCbk) (void*, stm_time64_t, uint16_t, uint16_t, stm_display_latency_params_t *),
                              void (* FrameCompleteCbk) (void*, const stm_buffer_presentation_stats_t *),
                              void *display_context)
{
    int Result = -1;
    unsigned long physAddr = Frame->BlitterAddr.base;

    stm_blitter_surface_format_t        BlitterFmt;
    stm_blitter_dimension_t             BlitterDim;
    stm_blitter_surface_colorspace_t    BlitterColorSpace;

    mutex_lock(&Display.lock);
    if (last_sebuffer == NULL) {
        mutex_unlock(&Display.lock);
        return -1;
    }

    memcpy(Frame, last_sebuffer, sizeof(struct SEBuffer));

    Frame->BlitterAddr.base = physAddr;
    Frame->DisplayBuffer.src.primary_picture.video_buffer_addr = physAddr;
    Frame->DisplayBuffer.info.puser_data = Frame;
    Frame->DisplayBuffer.info.display_callback = FrameCbk;
    //Frame->DisplayBuffer.info.completed_callback = &DefaultFinishedCallback;
    Frame->DisplayBuffer.info.completed_callback = FrameCompleteCbk;

    if (last_sebuffer->DisplayBuffer.src.primary_picture.color_fmt == SURF_YCbCr420R2B)
        BlitterFmt = STM_BLITTER_SF_NV12;
    else
        BlitterFmt = STM_BLITTER_SF_YCBCR420MB;

    BlitterColorSpace = STM_BLITTER_SCS_BT601;

    BlitterDim.w = last_sebuffer->BlitterDim.w;
    BlitterDim.h = last_sebuffer->BlitterDim.h;


    Frame->BlitterSurface = stm_blitter_surface_new_preallocated(
                    BlitterFmt,
                    BlitterColorSpace,
                    &Frame->BlitterAddr,
                    last_sebuffer->DisplayBuffer.src.primary_picture.video_buffer_size,
                    &BlitterDim,
                    last_sebuffer->DisplayBuffer.src.primary_picture.pitch);

        if (IS_ERR(Frame->BlitterSurface)) {
            Result = PTR_ERR(Frame->BlitterSurface);
            ERROR("stm_blitter_surface_new_preallocated failed for blitter input ! (%d)\n", Result);
            ERROR("[parameters]\n");
            ERROR("|- fmt=%x\n", BlitterFmt);
            ERROR("|- cs=%x\n", BlitterColorSpace);
            ERROR("|- addr.base=%lx\n", Frame->BlitterAddr.base);
            if (BlitterColorSpace != STM_BLITTER_SCS_RGB)
                ERROR("|- addr.cbcr_offset=%ld\n", Frame->BlitterAddr.cbcr_offset);
            ERROR("|- size=%d\n", last_sebuffer->DisplayBuffer.src.primary_picture.video_buffer_size);
            ERROR("|- width=%ld\n", BlitterDim.w);
            ERROR("|- height=%ld\n", BlitterDim.h);
            ERROR("|- stride=%d\n", last_sebuffer->DisplayBuffer.src.primary_picture.pitch);
           }

    Result |= omxse_blitter_performBliterring(last_sebuffer->BlitterSurface, Frame->BlitterSurface, &last_sebuffer->ResizedRect, &last_sebuffer->BlitterRect);

    mutex_unlock(&Display.lock);           //check what is this for

    if(Result != 0) {
        ERROR("OMXSE(display): blit operation for default frame display failed (%d)\n", Result);
    }

    Frame->SEUserPrivate = 0xFFFFFFFE;
    Result = displayDefaultFrame((struct omxse_display_context*)display_context, Frame);
    if(Result != 0) {
        ERROR("OMXSE(display): displayDefaultFrame failed (%d)\n", Result);
    }
    mutex_lock(&Display.lock);
    last_sebuffer = NULL;
    mutex_unlock(&Display.lock);
    return Result;
}

static struct cdev cdev;
static dev_t omxse_display_devno;

//============================================================================
//  file_operations
//     list of display module entry points
//============================================================================
static struct file_operations fops =
{
    .owner          = THIS_MODULE,
    .open           = omxse_display_open,
    .release        = omxse_display_release,
    .unlocked_ioctl = omxse_display_ioctl,
};

//============================================================================
//  omxse_display_init
//     called when display module is loaded
//============================================================================
int omxse_display_init(dev_t devno, struct class *omxse_class, const char* name)
{
    int Result;
    struct device * mdev;

    cdev_init(&cdev, &fops);
    Result = cdev_add(&cdev, devno, 1);
    if (0 != Result) {
        ERROR("cdev_add\n");
        return Result;
    }

    mdev = device_create(omxse_class, NULL, devno, NULL, name, MINOR(devno));
    if (IS_ERR(mdev)) {
        ERROR("device_create\n");
        cdev_del(&cdev);
        return PTR_ERR(mdev);
    }

    memset((void*)&Display, 0, sizeof(Display));
    mutex_init(&Display.lock);

    omxse_display_devno = devno;
    //INIT_WORK(&work, omxse_display_work);

    return 0;
}

//============================================================================
//  omxse_display_done
//     called when display module is unloaded
//============================================================================
void omxse_display_done(struct class *omxse_class)
{
    if(Display.Display != 0)
    {
        stm_display_device_close(Display.Display);
        Display.Display = 0;
    }

    device_destroy(omxse_class, omxse_display_devno);
    cdev_del(&cdev);
}

//============================================================================
//  omxse_display_flush
//============================================================================
void omxse_display_flush(void* display_context, int flush_buffers)
{
    int ret = 0;
    mutex_lock(&Display.lock);

    if(display_context)
    {
        if(((struct omxse_display_context*)display_context)->QueueInterface) {
            ret = stm_display_source_queue_flush (((struct omxse_display_context*)display_context)->QueueInterface, flush_buffers);
        }

        if (ret) {
            ERROR("OMXSE(display): omxse_display_flush:  stm_display_source_queue_flush() returned %d\n", ret);
        }
    }

    if (flush_buffers)
        last_sebuffer = NULL;

    mutex_unlock(&Display.lock);
}

//============================================================================
//  omxse_display_disabled
//============================================================================
bool omxse_display_disabled(void* display_context)
{
    // No lock since no impact if we grab one frame more or less
    if(display_context == NULL)
        return true;
    else
        return ((struct omxse_display_context*)display_context)->QueueInterface == NULL;
}

bool omxse_frame_displayed(void)
{
    if (last_sebuffer == NULL)
        return false;
    else
        return true;
}

