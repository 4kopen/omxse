/************************************************************************
Copyright (C) 2005 STMicroelectronics. All Rights Reserved.

This file is part of the Dvb_adaptation Library.

Dvb_adaptation is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Dvb_adaptation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Dvb_adaptation Library may alternatively be licensed under a proprietary
license from ST.

************************************************************************/

#include <linux/mm_types.h>
#include <linux/cdev.h>              /* character device definitions */
#include <linux/device.h>
#include <linux/wait.h>
#include <linux/sched.h>

#include <linux/stm/blitter.h>
#include <include/stmdisplay.h>
#include "stm_se.h"

/*
 * Init methods
 */
#define MODULE_NAME     "STM OMX streamer"

#if defined(OMXSE_VERBOSE)
#define VERBOSE(fmt, ...) printk(fmt,  ##__VA_ARGS__)
#else
#define VERBOSE(fmt, ...)
#endif

#if defined(OMXSE_DEBUG)
#define DEBUG(fmt, ...) printk(fmt,  ##__VA_ARGS__)
#else
#define DEBUG(fmt, ...)
#endif

#define ERROR(fmt, ...) printk(fmt,  ##__VA_ARGS__)
#define INFO(fmt, ...) printk(fmt,  ##__VA_ARGS__)

#define MAX_WIDTH_FOR_RESIZE  1920
#define MAX_HEIGHT_FOR_RESIZE 1080


typedef enum {
    STATE_OPEN,
    STATE_CONFIGURED,
    STATE_PLAY,
    STATE_STOPPED} state_t ;
/*
 * Buffer management
 */
struct SEBuffer {
    void*                               parentContext;
    state_t                            *parentState;
    struct mutex                       *parentLockStop;

    // Link with SE buffer
    releaseCaptureBuffer                SEReleaseBuffer;
    stm_se_event_context_h              SEReleaseBufferContext;
    unsigned int                        SEUserPrivate; // 0xFFFFFFFF mean freed

    // Bliterring configuration
    stm_blitter_surface_address_t       BlitterAddr;
    stm_blitter_rect_t                  BlitterRect;
    stm_blitter_surface_t              *BlitterSurface;
    stm_blitter_dimension_t             BlitterDim;
    stm_blitter_surface_format_t        BlitterFmt;
    stm_blitter_surface_colorspace_t    BlitterCS;
    stm_blitter_rect_t                  ResizedRect;

    // Display configuration
    stm_display_buffer_t                DisplayBuffer;

    // Video size
    bool                                isGrThanMaxForResize;

    struct list_head                    alllist;
    struct list_head					worklist;
};

struct OMXBuffer {
    void*                           parentContext;

    unsigned long                   userID;

    // Frame description
    unsigned long                   virtualAddr;
    unsigned long                   physicalAddr;
    unsigned long                   bufferSize;

    // Application buffer
    stm_blitter_surface_t           *AppBlitterSurface;
    stm_blitter_rect_t              AppRect;

    // Description valid only when buffer filled
    struct SEBuffer                 *sebuffer;
    struct SEBuffer                 *bltBuffer;

    unsigned long long              PTS;
    unsigned char                   eos;

    struct list_head                list;
    struct list_head                alllist;
};

void                    omxse_buffer_init(void);
void                    omxse_buffer_release(const char* componentName, void* parentContext);
void                    omxse_buffer_dissociate(const char* componentName, void* parentContext);
void                    omxse_buffer_registerOMX(const char* componentName, struct OMXBuffer* buffer);
void                    omxse_buffer_registerSE(const char* componentName, struct SEBuffer* sebuffer);
struct OMXBuffer*       omxse_buffer_findOMXByUserID(unsigned long userId);
struct OMXBuffer*       omxse_buffer_findOMXByPhysAddr(unsigned long physicalAddr);
struct SEBuffer*        omxse_buffer_findSE(unsigned long physicalAddr);
void                    omxse_buffer_delayedRelease(struct SEBuffer* sebuffer);

unsigned long GetPhysicalContiguous(unsigned long ptr, size_t size);

/*
 * Capture management
 */
int omxse_capture_init(dev_t devno, struct class *omxse_class, const char* name);
void omxse_capture_done(struct class *omxse_class);
void omxse_capture_ReleaseSEBuffer(struct SEBuffer* sebuffer);
void omxse_capture_ReleaseSEBufferCallback(struct SEBuffer* sebuffer);
void omxse_release_sebuffers(void* parentContext);
/*
 * Display management
 */
int omxse_blitter_init(dev_t devno, struct class *omxse_class, const char* name);
void omxse_blitter_done(struct class *omxse_class);
struct SEBuffer* omxse_buffer_stillALive(struct SEBuffer* sebuffer);
int omxse_blitter_performBliterring(stm_blitter_surface_t* src,stm_blitter_surface_t* dst,stm_blitter_rect_t *srcRect,stm_blitter_rect_t* dstRect);
int omxse_blitter_resizeDisplayBufferInPlace(struct OMXBuffer* buffer, int maxWidth, int maxHeight);

/*
 * Display management
 */
int omxse_display_init(dev_t devno, struct class *omxse_class, const char* name);
void omxse_display_done(struct class *omxse_class);
void omxse_display_flush(void* display_context, int flush_buffers);
bool omxse_display_disabled(void* display_context);
bool omxse_frame_displayed(void);
int omxse_save_display_state(struct SEBuffer *Frame,
                              void (* FrameCbk) (void*, stm_time64_t, uint16_t, uint16_t, stm_display_latency_params_t *),
                              void (* FrameCompleteCbk) (void*, const stm_buffer_presentation_stats_t *),
                              void *display_context);

/*
 * Vsync management
 */
int omxse_vsync_init(dev_t devno, struct class *omxse_class, const char* name);
void omxse_vsync_done(struct class *omxse_class);

