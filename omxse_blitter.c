/************************************************************************
Copyright (C) 2005 STMicroelectronics. All Rights Reserved.

This file is part of the Dvb_adaptation Library.

Dvb_adaptation is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Dvb_adaptation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Dvb_adaptation Library may alternatively be licensed under a proprietary
license from ST.

Source file name : omxse_display.c
Author :           Jean-Philippe Fassino

************************************************************************/

#include <linux/module.h>            /* kernel module definitions */
#include <linux/fs.h>                /* file system operations */
#include <linux/slab.h>
#include <linux/uaccess.h>

#include "stm_se.h"

#include "omxse.h"
#include "omxse_internal.h"

struct omxse_blitter_context {
    // Blittter
    stm_blitter_t                   *Blitter;
};

extern int colorBlitterFmt[];
extern int colorBlitterCs[];
extern int colorStride[];

struct omxse_blitter_context Blitter;

//============================================================================
//  omxse_blitter_performBliterring
//============================================================================
int omxse_blitter_performBliterring(stm_blitter_surface_t* src, stm_blitter_surface_t* dst, stm_blitter_rect_t* srcRect, stm_blitter_rect_t* dstRect)
{
    if (stm_blitter_surface_stretchblit(Blitter.Blitter,
             src, srcRect,
             dst, dstRect, 1) == 0) {
        stm_blitter_serial_t serial;
        stm_blitter_surface_get_serial(dst, &serial);
        stm_blitter_wait(Blitter.Blitter, STM_BLITTER_WAIT_SERIAL, serial);
    } else
    {
        ERROR("%s: Error during blitter operation\n", __FUNCTION__);
        return -EIO;
    }
    return 0;
}

int omxse_div_round(int num, int deno) {
    int ret = 0;
    if(deno == 0) {
        return ret;
    }
    ret = num/deno;
    if(num%deno >= num/2)
        ret++;
    return ret;
}

void omxse_calculateNewWidthHeight(int* width, int* height, int maxWidth, int maxHeight) {
    int w, h;
    w = *width;
    h = *height;
    *width = maxWidth;
    *height = omxse_div_round((h * *width), w);
    VERBOSE("%s[%d] w: %d, h: %d\n",__FUNCTION__, __LINE__, *width, *height);
    if(*height > maxHeight) {
        *height = maxHeight;
        *width = omxse_div_round((w * *height), h);
        VERBOSE("%s[%d] w: %d, h: %d\n",__FUNCTION__, __LINE__, *width, *height);
    }
}

int omxse_blitter_resizeDisplayBufferInPlace(struct OMXBuffer* buffer, int maxWidth, int maxHeight)
{
    struct SEBuffer* sebuffer;
    stm_display_buffer_t* displayBuffer;
    stm_blitter_surface_t *dstSurface;
    stm_blitter_rect_t dstRect;
    stm_blitter_surface_address_t dstAddr;

    int ret, width, height;

    sebuffer = buffer->sebuffer;
    displayBuffer = &buffer->sebuffer->DisplayBuffer;

    width = displayBuffer->src.primary_picture.width;
    height = displayBuffer->src.primary_picture.height;
    omxse_calculateNewWidthHeight(&width, &height, maxWidth, maxHeight);
    DEBUG("w: %d, h: %d from calculateNewWidthHeight\n",width, height);

    dstAddr.base = displayBuffer->src.primary_picture.video_buffer_addr;
    dstAddr.cbcr_offset = displayBuffer->src.primary_picture.chroma_buffer_offset;
    dstRect.position.x = 0;
    dstRect.position.y = 0;
    dstRect.size.w = width;
    dstRect.size.h = height;

    dstSurface = stm_blitter_surface_new_preallocated(
                sebuffer->BlitterFmt, sebuffer->BlitterCS,
                &dstAddr,
                displayBuffer->src.primary_picture.video_buffer_size,
                &dstRect.size,
                displayBuffer->src.primary_picture.pitch);

    if (IS_ERR(dstSurface)) {
        ret = PTR_ERR(dstSurface);
        ERROR("%s couldnot create dst blitter surface\n",__FUNCTION__);
        return ret;
    }

    ret = omxse_blitter_performBliterring(buffer->bltBuffer->BlitterSurface, dstSurface, &buffer->bltBuffer->BlitterRect, &dstRect);
    if(ret == 0) {
        sebuffer->ResizedRect.size.w = width;
        sebuffer->ResizedRect.size.h = height;
        sebuffer->DisplayBuffer.src.visible_area.width = width;
        sebuffer->DisplayBuffer.src.visible_area.height = height;
        sebuffer->DisplayBuffer.src.Rect.width  = width;
        sebuffer->DisplayBuffer.src.Rect.height = height;
    } else {
        ERROR("%s Error during blitter operation\n",__FUNCTION__);
    }
    stm_blitter_surface_put(dstSurface);
    return ret;
}

int omxse_blitter_performBliterringRaw(void __user *argp)
{
    omxse_raw_blit raw_blit;
    stm_blitter_surface_t* src;
    stm_blitter_surface_t* dst;
    stm_blitter_surface_address_t srcAddr;
    stm_blitter_surface_address_t dstAddr;
    stm_blitter_rect_t srcRect, dstRect;

    int ret = 0;

    if (copy_from_user(&raw_blit, argp, sizeof(raw_blit)))
        return -EFAULT;
    srcAddr.base = GetPhysicalContiguous(raw_blit.src.virtualAddr, raw_blit.src.bufferSize);
    dstAddr.base = GetPhysicalContiguous(raw_blit.dst.virtualAddr, raw_blit.dst.bufferSize);
    DEBUG("src-> vaddr: 0x%p, phyaddr: 0x%p\n",(void*)raw_blit.src.virtualAddr, (void*)srcAddr.base);
    DEBUG("dst-> vaddr: 0x%p, phyaddr: 0x%p\n",(void*)raw_blit.dst.virtualAddr, (void*)dstAddr.base);
    if((raw_blit.src.Color >= OMXSE_COLOR_MAX) ||
       (raw_blit.dst.Color >= OMXSE_COLOR_MAX))
    {
        ERROR("%s: Color Index is tainted \n", __FUNCTION__);
        return -EIO;
    }

    if (colorBlitterCs[raw_blit.src.Color] != STM_BLITTER_SCS_RGB)
        srcAddr.cbcr_offset = raw_blit.src.width * colorStride[raw_blit.src.Color] * raw_blit.src.height;
    if (colorBlitterCs[raw_blit.dst.Color] != STM_BLITTER_SCS_RGB)
        dstAddr.cbcr_offset = raw_blit.dst.width * colorStride[raw_blit.dst.Color] * raw_blit.dst.height;
    srcRect.position.x = 0;
    srcRect.position.y = 0;
    srcRect.size.w = raw_blit.src.width;
    srcRect.size.h = raw_blit.src.height;
    dstRect.position.x = 0;
    dstRect.position.y = 0;
    dstRect.size.w = raw_blit.dst.width;
    dstRect.size.h = raw_blit.dst.height;
    DEBUG("src-> w: %d, h: %d, cropw: %ld, croph: %ld\n", raw_blit.src.width, raw_blit.src.height, raw_blit.cropWidth, raw_blit.cropHeight);
    DEBUG("dst-> w: %d, h: %d\n", raw_blit.dst.width, raw_blit.dst.height);
    src = stm_blitter_surface_new_preallocated(
                colorBlitterFmt[raw_blit.src.Color], colorBlitterCs[raw_blit.src.Color],
                &srcAddr,
                raw_blit.src.bufferSize,
                &srcRect.size,
                raw_blit.src.width * colorStride[raw_blit.src.Color]);
    dst = stm_blitter_surface_new_preallocated(
                colorBlitterFmt[raw_blit.dst.Color], colorBlitterCs[raw_blit.dst.Color],
                &dstAddr,
                raw_blit.dst.bufferSize,
                &dstRect.size,
                raw_blit.dst.width * colorStride[raw_blit.dst.Color]);
    if ((raw_blit.cropWidth > 0) && (raw_blit.cropHeight >0)) {
        srcRect.position.x = 0;
        srcRect.position.y = 0;
        srcRect.size.w = raw_blit.cropWidth;
        srcRect.size.h = raw_blit.cropHeight;
    }
    if (stm_blitter_surface_stretchblit(Blitter.Blitter,
             src, &srcRect,
             dst, &dstRect, 1) == 0) {
        stm_blitter_serial_t serial;
        stm_blitter_surface_get_serial(dst, &serial);
        stm_blitter_wait(Blitter.Blitter, STM_BLITTER_WAIT_SERIAL, serial);
    } else
    {
        ERROR("%s: Error during blitter operation\n", __FUNCTION__);
        ret = -EIO;
    }
    stm_blitter_surface_put(src);
    stm_blitter_surface_put(dst);
    return ret;
}

//============================================================================
//  blitterClose
//============================================================================
static void blitterClose(void)
{
    if(Blitter.Blitter)
    {
        stm_blitter_put(Blitter.Blitter);
        Blitter.Blitter = NULL;
    }
}

//============================================================================
//  blitterOpen
//============================================================================
static int blitterOpen(void)
{
    int Result = -1;

    /*
     * Open Blitter
     */
    Blitter.Blitter = stm_blitter_get(0);
    if (IS_ERR(Blitter.Blitter)) {
        Result = PTR_ERR(Blitter.Blitter);
        ERROR("OMXSE(display): stm_blitter_get Failed (%d)\n", Result);
        goto error;
    }

    return 0;

error:
    blitterClose();
    return Result;
}



//============================================================================
//  omxse_blitter_open
//     call at module open
//============================================================================
static int omxse_blitter_open(struct inode *inode, struct file *filp)
{
    DEBUG("OMXSE(blitter): omxse_blitter_open %d\n", MINOR(inode->i_rdev));

    filp->private_data = (void*)&Blitter;
    filp->f_pos = 0;
    return 0;
}

//============================================================================
//  omxse_blitter_release
//     call at module close: release the blitter module
//============================================================================
static int omxse_blitter_release(struct inode *inode, struct file *filp)
{
    DEBUG("OMXSE(blitter): omxse_blitter_release\n");

    return 0;
}

//============================================================================
//  omxse_blitter_ioctl
//     management of IOCTLs blitter module
//============================================================================
static long omxse_blitter_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    switch(cmd)
    {
    case OMXSE_PERFORM_BLITTERING: {
        unsigned long physicalAddr;
        struct OMXBuffer* buffer;

        DEBUG("OMXSE(blitter): Blit\n");

        physicalAddr = GetPhysicalContiguous(arg, 1);
        if (!physicalAddr)
            return -EIO;

        buffer = omxse_buffer_findOMXByPhysAddr(physicalAddr);
        if(buffer != NULL && buffer->sebuffer != NULL)
        {
            return omxse_blitter_performBliterring(buffer->bltBuffer->BlitterSurface, buffer->AppBlitterSurface,
                                                   &buffer->bltBuffer->ResizedRect, &buffer->AppRect);
        }

        ERROR("OMXSE(blitter): OMXSE_PERFORM_BLITTERING on an buffer not associate with SE Buffer (%lx)\n", physicalAddr);
        return -EIO;
    }; break;

    case OMXSE_PERFORM_BLITTERING_RAW: {
        void __user *argp = (void __user *) arg;
        return omxse_blitter_performBliterringRaw(argp);
    }; break;

    default:
        ERROR("OMXSE(blitter): Unknow Ioctl\n");
        return -EINVAL;
    }

    return 0;
}

static struct cdev cdev;
static dev_t omxse_blitter_devno;

//============================================================================
//  file_operations
//     list of blitter module entry points
//============================================================================
static struct file_operations fops =
{
    .owner          = THIS_MODULE,
    .open           = omxse_blitter_open,
    .release        = omxse_blitter_release,
    .unlocked_ioctl = omxse_blitter_ioctl,
};


//============================================================================
//  omxse_blitter_init
//     called when blitter module is loaded
//============================================================================
int omxse_blitter_init(dev_t devno, struct class *omxse_class, const char* name)
{
    int err;
    struct device * mdev;

    cdev_init(&cdev, &fops);
    err = cdev_add(&cdev, devno, 1);
    if (0 != err) {
        ERROR("cdev_add\n");
        return err;
    }

    mdev = device_create(omxse_class, NULL, devno, NULL, name, MINOR(devno));
    if (IS_ERR(mdev)) {
        ERROR("device_create\n");
        cdev_del(&cdev);
        return PTR_ERR(mdev);
    }

    omxse_blitter_devno = devno;

    return blitterOpen();
}

//============================================================================
//  omxse_blitter_done
//     called when blitter module is unloaded
//============================================================================
void omxse_blitter_done(struct class *omxse_class)
{
    blitterClose();

    device_destroy(omxse_class, omxse_blitter_devno);
    cdev_del(&cdev);
}

