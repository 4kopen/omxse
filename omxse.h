/************************************************************************
Copyright (C) 2005 STMicroelectronics. All Rights Reserved.

This file is part of the Dvb_adaptation Library.

Dvb_adaptation is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Dvb_adaptation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Dvb_adaptation Library may alternatively be licensed under a proprietary
license from ST.

************************************************************************/

#ifndef OMX_MODULE_H_
#define OMX_MODULE_H_

#include <linux/types.h>

#define OMXSE_MAX_BUFFER 10
#define OMXSE_NAME_LENGTH 32

typedef enum {
    OMXSE_ENCODING_MPEG4P2,
    OMXSE_ENCODING_H264,
    OMXSE_ENCODING_H263,
    OMXSE_ENCODING_MPEG2,
    OMXSE_ENCODING_VC1,
    OMXSE_ENCODING_WMV,
    OMXSE_ENCODING_FLV1,
    OMXSE_ENCODING_MJPEG,
    OMXSE_ENCODING_VP8,
    OMXSE_ENCODING_VP9,
    OMXSE_ENCODING_THEORA,
    OMXSE_ENCODING_AVS,
    OMXSE_ENCODING_HEVC,
    OMXSE_ENCODING_MAX, // Do not add element after this
} omxse_encoding;

typedef enum {
    OMXSE_COLOR_ARGB8888,
    OMXSE_COLOR_BGRA8888,
    OMXSE_COLOR_RGB565,
    OMXSE_COLOR_NV12,
    OMXSE_COLOR_NV16,
    OMXSE_COLOR_NV21,
    OMXSE_COLOR_NV24,
    OMXSE_COLOR_YUV420_Y10_V10U10,
    OMXSE_COLOR_MAX, // Do not add element after this
} omxse_color;

typedef struct {
    omxse_encoding  Encoding;
    unsigned char   thumbnail;
    unsigned int    width;
    unsigned int    height;
    unsigned char   enable10Bit;
    char            componentName[OMXSE_NAME_LENGTH];
} omxse_setencoding;

typedef struct {
    unsigned long userID;
    unsigned long long PTS;
    unsigned char eos;
} omxse_grab;

typedef struct {
    unsigned long   userID;
    unsigned long   virtualAddr;
    unsigned int    bufferSize;
    unsigned int    width;
    unsigned int    height;
    omxse_color     Color;
} omxse_usebuffer;

typedef struct {
    char            planeName[OMXSE_NAME_LENGTH];
} omxse_setplane;

typedef struct {
    unsigned int inputCropX, inputCropY, inputCropW, inputCropH;
    unsigned int displayX, displayY, displayW, displayH;
} omxse_display_setoverlay;

typedef struct {
    unsigned long virtualAddr;
    int           displayRatio;
} omxse_queuevdp;

typedef struct {
    char planeName[OMXSE_NAME_LENGTH];
    unsigned int value;
} omxse_display_transparency;

typedef struct {
  omxse_usebuffer src;
  omxse_usebuffer dst;
  long cropWidth;
  long cropHeight;
 } omxse_raw_blit;

#define OMXSE_SET_ENCODING          _IO('o', 10)
#define OMXSE_USE_BUFFER            _IO('o', 11)

#define OMXSE_START                 _IO('o', 20)
#define OMXSE_STOP                  _IO('o', 21)

#define OMXSE_FILL_THIS_BUFFER      _IO('o', 30)
#define OMXSE_FILL_BUFFER_DONE      _IO('o', 31)
#define OMXSE_FLUSH_BUFFER          _IO('o', 32)
#define OMXSE_INJECT_DISCONTINUITY  _IO('o', 33)
#define OMXSE_INJECT_EOS            _IO('o', 34)

#define OMXSE_SET_PLANE             _IO('o', 40)
#define OMXSE_IS_VDP_BUFFER         _IO('o', 41)
#define OMXSE_QUEUE_VDP             _IO('o', 42)
#define OMXSE_SET_OVERLAY           _IO('o', 43)
#define OMXSE_SET_PLANE_TRANSPARENCY _IO('o', 44)

#define OMXSE_PERFORM_BLITTERING    _IO('o', 50)
#define OMXSE_PERFORM_BLITTERING_RAW _IO('o', 51)

#define OMXSE_WAIT_FOR_VSYNC        _IO('o', 60)

#endif /* OMX_MODULE_H_ */
