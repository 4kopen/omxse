EXTRA_CFLAGS += -Werror

EXTRA_CFLAGS+=-I$(TREE_ROOT)/linux/include/
EXTRA_CFLAGS+=-I$(TREE_ROOT)/linux/include/linux/dvb/

EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/include/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/include/linux/stm/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/media/dvb/stm/dvb/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/media/dvb/stm/allocator/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/media/dvb/stm/h264_preprocessor/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/sound/pseudocard/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/sound/ksound/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/sound/pcm_transcoder/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/stm/mmelog/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/stm/monitor/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/stm/st_relay/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/media/sysfs/stm/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/drivers/osdev_abs
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/linux/components/inline/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/components/include/
EXTRA_CFLAGS += -I$(CONFIG_PLAYER2_PATH)/player/standards/
EXTRA_CFLAGS += -I$(CONFIG_PLAYER2_PATH)/player/wrapper/
EXTRA_CFLAGS+=-I$(CONFIG_PLAYER2_PATH)/player/include/

EXTRA_CFLAGS += -I$(CONFIG_STGFB_PATH)
EXTRA_CFLAGS += -I$(CONFIG_STGFB_PATH)/include
## Linux2.4 paths
EXTRA_CFLAGS += -I$(CONFIG_STGFB_PATH)/linux/kernel
EXTRA_CFLAGS += -I$(CONFIG_STGFB_PATH)/linux/kernel/include/
EXTRA_CFLAGS += -I$(CONFIG_STGFB_PATH)/linux/kernel/drivers/media/video

EXTRA_CFLAGS += -I$(CONFIG_BLITTER_PATH)/linux/kernel/include

EXTRA_CFLAGS += -I$(CONFIG_KERNEL_PATH)/drivers/media/dvb/dvb-core
EXTRA_CFLAGS += -I$(CONFIG_MULTICOM_PATH)/include
EXTRA_CFLAGS += -I$(CONFIG_MULTICOM_PATH)/source/include

EXTRA_CFLAGS += -I$(CONFIG_INFRA_PATH)/include

EXTRA_CFLAGS += -I$(INSTALL_MOD_PATH)/usr/include

#-DOMXSE_DEBUG
#-DOMXSE_VERBOSE
#EXTRA_CFLAGS += \
#	-DOMXSE_DEBUG \
#	-DOMXSE_VERBOSE

obj-m += stmomx.o

stmomx-objs:=   \
		omxse_buffer.o \
		omxse_blitter.o \
		omxse_display.o \
		omxse_capture.o \
		omxse_vsync.o \
		omx_module.o
