/************************************************************************
Copyright (C) 2005 STMicroelectronics. All Rights Reserved.

This file is part of the Dvb_adaptation Library.

Dvb_adaptation is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Dvb_adaptation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Dvb_adaptation Library may alternatively be licensed under a proprietary
license from ST.

Source file name : omxse_capture.c
Author :           Jean-Philippe Fassino

************************************************************************/

#include <linux/module.h>            /* kernel module definitions */
#include <linux/fs.h>                /* file system operations */
#include <linux/slab.h>
#include <linux/uaccess.h>

#include "omxse.h"
#include "omxse_internal.h"
#include "osdev_device.h"

struct omxse_context {
    struct mutex                    lock;
    struct mutex                    lockStop;

    // Blittter
    stm_blitter_t                   *Blitter;
    void*                            displayContext;

    // Player2 context
    stm_se_playback_h               Playback;
    char                            StreamName[32];
    stm_se_play_stream_h            Stream;
    char                            SinkName[32];
    stm_object_h                    Sink;

    state_t                         state;
    unsigned int                    thumbnail;
    unsigned int                    injectionPending;
    unsigned int                    displayPending;

    // Input
    stm_se_stream_encoding_t        Encoding;
    unsigned int                    width, height;
    bool                            enable10Bit;
    char                            componentName[OMXSE_NAME_LENGTH];

    // Stats
    int                             numberOfCapturedFrame;
    int                             numberOfNotDisplayedFrame;

    char*                           kernBuffer;
    int                             kernBufferSize;

    // Empty buffer to return to player when EOS marker is received
    struct OMXBuffer bufferEos;

    struct list_head                emptyBufferList;
    struct list_head                filledBufferList;

    wait_queue_head_t               grabDone;
    wait_queue_head_t               injectionDone;
    wait_queue_head_t               emptyBuffersEvent;
    wait_queue_head_t               displayDone;
};


static stm_se_stream_encoding_t Encodings[] = {
        [OMXSE_ENCODING_MPEG4P2] = STM_SE_STREAM_ENCODING_VIDEO_MPEG4P2,
        [OMXSE_ENCODING_H264] = STM_SE_STREAM_ENCODING_VIDEO_H264,
        [OMXSE_ENCODING_H263] = STM_SE_STREAM_ENCODING_VIDEO_H263,
        [OMXSE_ENCODING_MPEG2] = STM_SE_STREAM_ENCODING_VIDEO_MPEG2,
        [OMXSE_ENCODING_VC1] = STM_SE_STREAM_ENCODING_VIDEO_VC1,
        [OMXSE_ENCODING_WMV] = STM_SE_STREAM_ENCODING_VIDEO_WMV,
        [OMXSE_ENCODING_FLV1] = STM_SE_STREAM_ENCODING_VIDEO_FLV1,
        [OMXSE_ENCODING_MJPEG] = STM_SE_STREAM_ENCODING_VIDEO_MJPEG,
        [OMXSE_ENCODING_VP8] = STM_SE_STREAM_ENCODING_VIDEO_VP8,
        [OMXSE_ENCODING_VP9] = STM_SE_STREAM_ENCODING_VIDEO_VP9,
        [OMXSE_ENCODING_THEORA] = STM_SE_STREAM_ENCODING_VIDEO_THEORA,
        [OMXSE_ENCODING_AVS] = STM_SE_STREAM_ENCODING_VIDEO_AVS,
        [OMXSE_ENCODING_HEVC] = STM_SE_STREAM_ENCODING_VIDEO_HEVC,
};

int colorBlitterFmt[] = {
        [OMXSE_COLOR_ARGB8888] = STM_BLITTER_SF_ABGR,
        [OMXSE_COLOR_BGRA8888] = STM_BLITTER_SF_ARGB,
        [OMXSE_COLOR_RGB565] = STM_BLITTER_SF_RGB565,
        [OMXSE_COLOR_NV12] = STM_BLITTER_SF_NV12,
        [OMXSE_COLOR_NV16] = STM_BLITTER_SF_NV16,
        [OMXSE_COLOR_NV21] = STM_BLITTER_SF_NV21,
        [OMXSE_COLOR_NV24] = STM_BLITTER_SF_NV24,
        [OMXSE_COLOR_YUV420_Y10_V10U10] = STM_BLITTER_SF_NV12_10B,
};
int colorBlitterCs[] = {
        [OMXSE_COLOR_ARGB8888] = STM_BLITTER_SCS_RGB,
        [OMXSE_COLOR_BGRA8888] = STM_BLITTER_SCS_RGB,
        [OMXSE_COLOR_RGB565] = STM_BLITTER_SCS_RGB,
        [OMXSE_COLOR_NV12] = STM_BLITTER_SCS_BT601,
        [OMXSE_COLOR_NV16] = STM_BLITTER_SCS_BT601,
        [OMXSE_COLOR_NV21] = STM_BLITTER_SCS_BT601,
        [OMXSE_COLOR_NV24] = STM_BLITTER_SCS_BT601,
        // TBD : Check for 10B
        [OMXSE_COLOR_YUV420_Y10_V10U10] = STM_BLITTER_SCS_BT601,
};

int colorStride[] = {
        [OMXSE_COLOR_ARGB8888] = 32 / 8,
        [OMXSE_COLOR_BGRA8888] = 32 / 8,
        [OMXSE_COLOR_RGB565] = 16 / 8,
        [OMXSE_COLOR_NV12] = 1,
        [OMXSE_COLOR_NV16] = 1,
        [OMXSE_COLOR_NV21] = 1,
        [OMXSE_COLOR_NV24] = 1,
        // TBD : need value for 10/8, so either change it to float and use macros
        [OMXSE_COLOR_YUV420_Y10_V10U10] = 1,
};

#define MAX_SUPPORTED_WIDTH_HD   1920
#define MAX_SUPPORTED_HEIGHT_HD  1080
#define MAX_SUPPORTED_WIDTH_UHD  3840
#define MAX_SUPPORTED_HEIGHT_UHD 2160

#define DISPLAY_BUF_BPA2_PARTITION "BPA2_Region0"
//changes in buff size to support 4K2K
// *(5/4) to accomodate 10 Bit buffers
#define DISPLAY_BUF_SIZE ((4096*2048*3*5)/(2*4))

void            *defaultFrameVirtAddr;
struct SEBuffer *defaultFrameDisplay;





#define MB64    (64 * 1024 * 1024)

static void          *alloc_blitter_constraint (char                    *Partition,
        unsigned int             Size,
        bool                     BoundedAllocation)
{
    struct bpa2_part    *partition;
    unsigned int         numpages;
    unsigned long        p = 0;
    unsigned long addrs[100];
    int index;

    partition   = bpa2_find_part(Partition);
    if( partition == NULL )
        return NULL;

    numpages    = (Size + PAGE_SIZE - 1) / PAGE_SIZE;
    p           = bpa2_alloc_pages( partition, numpages, 0, 0 /*priority*/ );

    if (p == 0)
        return NULL;

    if(!BoundedAllocation)
    {
        // No Alignment needed. Return from here
        return (void *)p;
    }

    for(index = 0; index < sizeof(addrs) / sizeof(addrs[0]); index++) {

        if (p == 0)
            break;

        if(p / MB64 != (p + Size) / MB64) {
            // We cross 64MB memory, free chunck and try to allocate a chunk from start to upper boundary
            int sizetoboundary = (p / MB64 + 1) * MB64 - p;

            bpa2_free_pages(partition, p);
            p = 0;

            addrs[index] = bpa2_alloc_pages(partition,
                                            (sizetoboundary + (PAGE_SIZE -1)) / PAGE_SIZE, 1, GFP_KERNEL);

        }
        else
            break; // We have found a good chunk, exit
    }

    for(; index-- > 0; )
        bpa2_free_pages(partition,  addrs[index]);


    return (void *)p;
}

static int free_display_buffer(void *Address , char *Partition)
{
    struct bpa2_part    *partition;

    partition   = bpa2_find_part(Partition);
    if (partition == NULL || Address == NULL)
        return -1;

    bpa2_free_pages( partition, (unsigned int)Address );

    if (!bpa2_low_part(partition)) {
        iounmap(Address);
    }
    return 0;
}

static void            *alloc_display_buffer(unsigned int             Allignment,
        unsigned int             Size,
        char                    *Partition,
        unsigned long           *PhysicalAddress)
{
    void    *Base;
    void    *Result;
    struct bpa2_part    *partitionStruct;

    partitionStruct   = bpa2_find_part(Partition);

    if (partitionStruct == NULL)
        return NULL;

    Result = NULL;
    Size   = Size + sizeof(void *) + Allignment - 1;

    /* No need for any Bounded Allocation for Cannes based devices */
    Base   = alloc_blitter_constraint(Partition, Size, false);

    *PhysicalAddress = (unsigned long)Base;

    if (Base != NULL) {
        if (bpa2_low_part(partitionStruct)) {
            Result = phys_to_virt(*PhysicalAddress);
        }
        else {
            Result = ioremap_wc(*PhysicalAddress, (unsigned long) Size);
        }
    }

    return Result;
}


void omxse_capture_setdisplay(struct SEBuffer* sebuffer, void *display_context)
{
  struct omxse_context* Context = (struct omxse_context*)sebuffer->parentContext;
  Context->displayContext = display_context;
}



static void DefaultFrameCallback  (void*                         Buffer,
                                   stm_time64_t                  vsync_time,
                                   uint16_t                      output_change,
                                   uint16_t                      nb_planes,
                                   stm_display_latency_params_t *display_latency_params)
{
  struct SEBuffer* sebuffer = (struct SEBuffer*)Buffer;
  struct omxse_context* Context = (struct omxse_context*)sebuffer->parentContext;
  Context->displayPending = 0;
  wake_up_interruptible(&Context->displayDone);
}

static void DefaultCompleteCallback  (void*                         Buffer,
                                   const stm_buffer_presentation_stats_t *Data)
{
  struct SEBuffer* sebuffer = (struct SEBuffer*)Buffer;
  stm_blitter_surface_put(sebuffer->BlitterSurface);
}

static uint32_t omxseGetMemProfile(struct omxse_context* Context)
{
    uint32_t ctrlValue = STM_SE_CTRL_VALUE_VIDEO_DECODE_HD_PROFILE;

    if((Context->Encoding == STM_SE_STREAM_ENCODING_VIDEO_HEVC) ||
        (Context->Encoding == STM_SE_STREAM_ENCODING_VIDEO_H264))
    {
        if(Context->width > MAX_SUPPORTED_WIDTH_UHD || Context->height > MAX_SUPPORTED_HEIGHT_UHD)
        {
            ctrlValue = STM_SE_CTRL_VALUE_VIDEO_DECODE_4K2K_PROFILE;
            if(Context->enable10Bit && Context->Encoding == STM_SE_STREAM_ENCODING_VIDEO_HEVC)
            {
                ctrlValue = STM_SE_CTRL_VALUE_VIDEO_DECODE_4K2K_10BITS_PROFILE;
            }
        } else if(Context->width > MAX_SUPPORTED_WIDTH_HD || Context->height > MAX_SUPPORTED_HEIGHT_HD)
        {
            ctrlValue = STM_SE_CTRL_VALUE_VIDEO_DECODE_UHD_PROFILE;
            if(Context->enable10Bit && Context->Encoding == STM_SE_STREAM_ENCODING_VIDEO_HEVC)
            {
                ctrlValue = STM_SE_CTRL_VALUE_VIDEO_DECODE_UHD_10BITS_PROFILE;
            }
        }
        else
        {
            if(Context->enable10Bit && Context->Encoding == STM_SE_STREAM_ENCODING_VIDEO_HEVC)
            {
                ctrlValue = STM_SE_CTRL_VALUE_VIDEO_DECODE_HD_10BITS_PROFILE;
            }
        }
    }
    return ctrlValue;
}

//============================================================================
//  captureStop
//  resource deallocation when playback is stopped
//============================================================================
static void captureStop(struct omxse_context* Context)
{
    int ret = 0;

    mutex_lock(&Context->lockStop);
    if(Context->Stream != NULL)
    {
        if(Context->Sink != NULL)
        {
            if(Context->state == STATE_PLAY)
            {
                /* Mutex shouldn't be taken during all this sequence because it can result in a dead lock
                 * Especially with callback.
                 */
                mutex_unlock(&Context->lock);

                /* Set state stopped at beginning for checking in injection (write) */
                Context->state = STATE_STOPPED;

                // Remove association between OMX buffers and omxse buffers
                omxse_buffer_dissociate(Context->componentName, Context);

                // Discard previously injected data
                stm_se_play_stream_drain(Context->Stream, true);

                // Flush Display in order to give back buffer to SE
                // No reason to close display neither to flush frames on display
                // last_sebuffer is checked in omxse_save_display_state
                if(!Context->thumbnail && !omxse_display_disabled(Context->displayContext)) {
                  Context->displayPending = 1;
                    if(defaultFrameVirtAddr == NULL)
                    {
                        unsigned long defaultFramePhysAddr;
                        defaultFrameVirtAddr = alloc_display_buffer(8, DISPLAY_BUF_SIZE, DISPLAY_BUF_BPA2_PARTITION, (unsigned long *) &defaultFramePhysAddr);

                        if (defaultFrameVirtAddr == NULL) {
                            ERROR("Out of memory for Display Buffer\n");
                            memset(defaultFrameDisplay, 0, sizeof(struct SEBuffer));
                            goto display_flush;
                        }

                        defaultFrameDisplay->BlitterAddr.base = defaultFramePhysAddr;
                        defaultFrameDisplay->DisplayBuffer.src.primary_picture.video_buffer_addr = defaultFramePhysAddr;
                        defaultFrameDisplay->DisplayBuffer.info.puser_data = defaultFrameDisplay;
                    }

                  memset(defaultFrameVirtAddr, 0, DISPLAY_BUF_SIZE);
                  ret = omxse_save_display_state(defaultFrameDisplay, &DefaultFrameCallback, &DefaultCompleteCallback, Context->displayContext);
                  if(ret) {
                      Context->displayPending = 0;
                      INFO("Error in omxse_save_display_state - displayDefaultFrame\n");
                  }
                  else {
                      wait_event_interruptible(Context->displayDone, (Context->displayPending == 0));
                  }
                }
display_flush:
                omxse_display_flush(Context->displayContext,0);

                stm_se_play_stream_register_buffer_capture_callback(
                        Context->Stream,
                        (stm_se_event_context_h)NULL,
                        (stream_buffer_capture_callback)NULL);

                // Wake up in case we are stuck in Grab !!
                wake_up_interruptible(&Context->grabDone);
                //to release the sebuffer which are still alive/not yet released by display
                omxse_release_sebuffers(Context);

                /* If injection is ongoing wait for injection done before continue
                 * Only possible case here is that injection was stuck because of no more buffers available
                 * Streaming engine has armed a timer and new injection try will occur at that time
                 * Injection will not be blocked because of the drain requested.
                 * The injectionPending state does not need to be protected: the point is to be sure to receive the
                 * event in case the injection was blocked. As this event is sytematically sent as soon as stop state is detected
                 * there is no race condition possible
                 */
                if(Context->injectionPending)
                {
                    INFO("%s:: waiting for injection to terminate\n", Context->componentName);
                    wait_event_interruptible(Context->injectionDone, (Context->injectionPending == false));
                }

                mutex_lock(&Context->lock);
            }

            if(stm_se_play_stream_detach(Context->Stream, Context->Sink))
                ERROR("%s:: stm_se_play_stream_detach Failed\n", Context->componentName);

            if(stm_se_play_stream_delete(Context->Stream) != 0)
                ERROR("%s:: stm_se_play_stream_delete Failed\n", Context->componentName);

            if(stm_registry_remove_object(Context->Sink) != 0)
                ERROR("%s:: stm_registry_remove_object Failed\n", Context->componentName);

            //if(stm_se_video_display_delete(Context->Sink) != 0)
                //ERROR("%s:: stm_se_video_display_delete Failed\n", Context->componentName);

            Context->Sink = NULL;
        }
        else
            if(stm_se_play_stream_delete(Context->Stream) != 0)
                ERROR("%s:: stm_se_play_stream_delete Failed\n", Context->componentName);

        Context->Stream = NULL;
    }
    if(Context->kernBuffer)
    {
        kfree(Context->kernBuffer);
        Context->kernBuffer = NULL;
        Context->kernBufferSize = 0;
    }
    mutex_unlock(&Context->lockStop);
}

//============================================================================
//  captureClose
//============================================================================
static void captureClose(struct omxse_context* Context)
{
    if(Context->Playback != NULL)
    {
        if(stm_se_playback_delete(Context->Playback) != 0)
            ERROR("%s:: stm_se_playback_delete Failed\n", Context->componentName);
    }

    kfree(Context);
}

//============================================================================
//  omxse_capture_ReleaseSEBuffer
//============================================================================
void omxse_capture_ReleaseSEBuffer(struct SEBuffer* sebuffer)
{
     if (omxse_buffer_stillALive(sebuffer))
     {
        if(sebuffer->SEUserPrivate != 0xFFFFFFFF)
        {
            sebuffer->SEReleaseBuffer(
                sebuffer->SEReleaseBufferContext,
                sebuffer->SEUserPrivate);
            sebuffer->SEUserPrivate = 0xFFFFFFFF;
        }
    }
}

void omxse_capture_ReleaseSEBufferCallback(struct SEBuffer* sebuffer)
{
    omxse_buffer_delayedRelease(sebuffer);
}

#include <linux/delay.h>


//============================================================================
//  seBufferAllocationAndInitialization
//    Allocation and initialization of a physical SE buffer
//============================================================================
static int seBufferAllocationAndInitialization(   struct omxse_context *Context,
                                                  struct capture_buffer_s* capture_buffer,
                                                  struct SEBuffer **sebuffer_p)
{
        stm_blitter_dimension_t             BlitterDim;
        struct SEBuffer *sebuffer;

        int Result = 0;

        *sebuffer_p = kzalloc(sizeof(struct SEBuffer), GFP_KERNEL);
        sebuffer = *sebuffer_p;

        if(sebuffer == NULL) {
            ERROR("Out of memory for allocating SE Buffer\n");
            Result =  1;
            goto FUNCTION_END;
        }
        sebuffer->SEUserPrivate = 0xFFFFFFFF;
        sebuffer->parentContext = Context;
        sebuffer->parentState = &Context->state;
        sebuffer->parentLockStop = &Context->lockStop;

        switch (capture_buffer->flags & (CAPTURE_XY_IN_32NDS | CAPTURE_XY_IN_16THS)) {
        case CAPTURE_XY_IN_32NDS:
            sebuffer->BlitterRect.position.x = capture_buffer->rectangle.x >> 1;
            sebuffer->BlitterRect.position.y = capture_buffer->rectangle.y >> 1;
            break;
        case CAPTURE_XY_IN_16THS:
            sebuffer->BlitterRect.position.x = capture_buffer->rectangle.x;
            sebuffer->BlitterRect.position.y = capture_buffer->rectangle.y;
            break;
        case 0:
            sebuffer->BlitterRect.position.x = capture_buffer->rectangle.x << (1 * 16);
            sebuffer->BlitterRect.position.y = capture_buffer->rectangle.y << (1 * 16);
            break;
        default:
            ERROR("%s: Unsupported capture flags %x\n", __FUNCTION__, capture_buffer->flags);
        }
        sebuffer->BlitterRect.size.w = capture_buffer->rectangle.width;
        sebuffer->BlitterRect.size.h = capture_buffer->rectangle.height;

        // Ensure no loss in resolution for pixel_depth/8 in case to 10bit buffer
        sebuffer->BlitterDim.w = BlitterDim.w = ((capture_buffer->stride << 3)/ capture_buffer->pixel_depth);
        sebuffer->BlitterDim.h = BlitterDim.h = capture_buffer->total_lines;


        // Valid since we are in SURFACE_FORMAT_VIDEO_420_RASTER2B/10B or in SURFACE_FORMAT_VIDEO_420_MACROBLOCK
        if(capture_buffer->buffer_format == SURFACE_FORMAT_VIDEO_420_RASTER2B)
        {
            sebuffer->BlitterFmt = STM_BLITTER_SF_NV12;
        }
        else if (capture_buffer->buffer_format == SURFACE_FORMAT_VIDEO_420_RASTER2B_10B)
        {
            sebuffer->BlitterFmt = STM_BLITTER_SF_NV12_10B;
        }
        else
        {
            sebuffer->BlitterFmt = STM_BLITTER_SF_YCBCR420MB;
        }
        sebuffer->BlitterCS = STM_BLITTER_SCS_BT601;


        sebuffer->BlitterAddr.base = capture_buffer->physical_address;
        sebuffer->BlitterAddr.cbcr_offset = capture_buffer->chroma_buffer_offset;

        sebuffer->SEReleaseBuffer = capture_buffer->releaseBuffer;
        sebuffer->SEReleaseBufferContext = capture_buffer->releaseBufferContext;

        sebuffer->DisplayBuffer.src.primary_picture.video_buffer_addr = capture_buffer->physical_address;
        sebuffer->DisplayBuffer.src.primary_picture.video_buffer_size = capture_buffer->size;
        sebuffer->DisplayBuffer.src.primary_picture.chroma_buffer_offset = capture_buffer->chroma_buffer_offset;

        if(capture_buffer->buffer_format == SURFACE_FORMAT_VIDEO_420_RASTER2B)
        {
            sebuffer->DisplayBuffer.src.primary_picture.color_fmt = SURF_YCbCr420R2B;
        }
        else if(capture_buffer->buffer_format == SURFACE_FORMAT_VIDEO_420_RASTER2B_10B)
        {
            sebuffer->DisplayBuffer.src.primary_picture.color_fmt = SURF_YCbCr420R2B10;
        }
        else
        {
            sebuffer->DisplayBuffer.src.primary_picture.color_fmt = SURF_YCBCR420MB;
        }

        sebuffer->DisplayBuffer.src.primary_picture.pixel_depth = capture_buffer->pixel_depth;

        sebuffer->DisplayBuffer.src.primary_picture.pitch = capture_buffer->stride;
        sebuffer->DisplayBuffer.src.primary_picture.width = capture_buffer->rectangle.width;
        sebuffer->DisplayBuffer.src.primary_picture.height = capture_buffer->rectangle.height;

        sebuffer->DisplayBuffer.src.visible_area.x = 0;
        sebuffer->DisplayBuffer.src.visible_area.y = 0;
        sebuffer->DisplayBuffer.src.visible_area.width = capture_buffer->rectangle.width;
        sebuffer->DisplayBuffer.src.visible_area.height = capture_buffer->rectangle.height;

        sebuffer->DisplayBuffer.src.ulConstAlpha = 0xff;
        sebuffer->DisplayBuffer.src.flags =
                STM_BUFFER_SRC_CONST_ALPHA |
                ((capture_buffer->flags & CAPTURE_COLOURSPACE_709) ? STM_BUFFER_SRC_COLORSPACE_709 : 0);

        /* Specify that the buffer is persistent, this allows us to pause
           video by simply not queuing any more buffers and the last frame
           stays on screen.*/
        sebuffer->DisplayBuffer.info.ulFlags = STM_BUFFER_PRESENTATION_PERSISTENT;
        /* Need nfields = n to display it n times, a value of 0=> do not display this frame */
        /* set it to 1 so that it is displayed at least once*/
        sebuffer->DisplayBuffer.info.nfields = 1;

        sebuffer->DisplayBuffer.info.display_callback = NULL;
        sebuffer->DisplayBuffer.info.completed_callback = NULL;
        sebuffer->DisplayBuffer.info.puser_data = sebuffer;

        sebuffer->DisplayBuffer.src.Rect.x      = 0;
        sebuffer->DisplayBuffer.src.Rect.y      = 0;
        sebuffer->DisplayBuffer.src.Rect.width  = capture_buffer->rectangle.width;
        sebuffer->DisplayBuffer.src.Rect.height = capture_buffer->rectangle.height;

        omxse_buffer_registerSE(Context->componentName, sebuffer);

        sebuffer->BlitterSurface = stm_blitter_surface_new_preallocated(
                sebuffer->BlitterFmt,
                sebuffer->BlitterCS,
                &sebuffer->BlitterAddr,
                capture_buffer->size,
                &BlitterDim,
                capture_buffer->stride);

        if (IS_ERR(sebuffer->BlitterSurface)) {
            Result = PTR_ERR(sebuffer->BlitterSurface);
            ERROR("stm_blitter_surface_new_preallocated failed for blitter input ! (%d)\n", Result);
            ERROR("[parameters]\n");
            ERROR("|- fmt=%x\n", sebuffer->BlitterFmt);
            ERROR("|- cs=%x\n", sebuffer->BlitterCS);
            ERROR("|- addr.base=%lx\n", sebuffer->BlitterAddr.base);
            if (sebuffer->BlitterCS != STM_BLITTER_SCS_RGB)
                ERROR("|- addr.cbcr_offset=%ld\n", sebuffer->BlitterAddr.cbcr_offset);
            ERROR("|- size=%d\n", capture_buffer->size);
            ERROR("|- width=%ld\n", BlitterDim.w);
            ERROR("|- height=%ld\n", BlitterDim.h);
            ERROR("|- stride=%d\n", capture_buffer->stride);
            Result = 1;

        }

        if (capture_buffer->rectangle.width > MAX_WIDTH_FOR_RESIZE || capture_buffer->rectangle.height > MAX_HEIGHT_FOR_RESIZE)
            sebuffer->isGrThanMaxForResize = true;
        else
            sebuffer->isGrThanMaxForResize = false;

FUNCTION_END:
        return Result;
}

//============================================================================
//  captureCallback
//    called as callback by SE to supply a decoded image
//============================================================================
static int captureCallback(
        struct omxse_context *Context,
        struct capture_buffer_s* capture_buffer)
{
    struct SEBuffer *sebuffer;
    struct OMXBuffer *buffer;
    int needStep;
    int Result = 0;

    if (capture_buffer == NULL)
    {
       ERROR("=========== captureCallback : FAIL !!!!! \n");
       Result = 1;
       goto FUNCTION_END;
    }

    if (capture_buffer->size == 0)
    {
       // EOS marker received: return an empty buffer to player

       mutex_lock(&Context->lock);
       Context->bufferEos.sebuffer = 0;
       Context->bufferEos.PTS = 0;
       Context->bufferEos.eos = 1;
       Context->bufferEos.userID = 0;
       mutex_unlock(&Context->lock);
       list_add_tail(&(Context->bufferEos.list), &Context->filledBufferList);

       wake_up_interruptible(&Context->grabDone);
    }
    else
    {
       // new frame received: Associate SE buffer with OMX buffer
       VERBOSE("Receive a new frame %x (%lx)\n", (unsigned int)Context, capture_buffer->physical_address);

       if((capture_buffer->buffer_format != SURFACE_FORMAT_VIDEO_420_RASTER2B) && (capture_buffer->buffer_format != SURFACE_FORMAT_VIDEO_420_MACROBLOCK)
          && (capture_buffer->buffer_format != SURFACE_FORMAT_VIDEO_420_RASTER2B_10B))
       {
           ERROR("Format != SURFACE_FORMAT_VIDEO_420_RASTER2B && Format != SURFACE_FORMAT_VIDEO_420_MACROBLOCK && Format != SURFACE_FORMAT_VIDEO_420_RASTER2B_10B\n");
           Result = 1;
           goto FUNCTION_END;
       }

       mutex_lock(&Context->lock);

       if(list_empty(&Context->emptyBufferList))
       {
           ERROR("capture_callback received, but no frame available; probably internal issue !!\n");
           mutex_unlock(&Context->lock);
           Result = 1;
           goto FUNCTION_END;
       }

       sebuffer = omxse_buffer_findSE(capture_buffer->physical_address);
       if(sebuffer == NULL)
       {
           // physical buffer not yet associated to player pseudo buffer
           Result = seBufferAllocationAndInitialization(Context, capture_buffer, &sebuffer);
           if(Result != 0)
           {
               mutex_unlock(&Context->lock);
               goto FUNCTION_END;
           }
       }
       else if(sebuffer->DisplayBuffer.src.primary_picture.width != capture_buffer->rectangle.width ||
               sebuffer->DisplayBuffer.src.primary_picture.height != capture_buffer->rectangle.height) {
           INFO("width: %d->%d height: %d -> %d\n",(int)sebuffer->DisplayBuffer.src.primary_picture.width, (int)capture_buffer->rectangle.width,
                                                   (int)sebuffer->DisplayBuffer.src.primary_picture.height, (int)capture_buffer->rectangle.height);
           stm_blitter_surface_put(sebuffer->BlitterSurface);
           list_del(&sebuffer->alllist);
           kfree(sebuffer);
           Result = seBufferAllocationAndInitialization(Context, capture_buffer, &sebuffer);
           if(Result != 0)
           {
               mutex_unlock(&Context->lock);
               goto FUNCTION_END;
           }
       }

       sebuffer->SEUserPrivate = capture_buffer->user_private;

       if(sebuffer->SEUserPrivate != 0xFFFFFFFF)
           ERROR("OMXSE Error Condition =%p, %x\n",sebuffer,sebuffer->SEUserPrivate);

       buffer = list_first_entry(&Context->emptyBufferList, struct OMXBuffer, list);

       list_del(&buffer->list);

       // Associate SE buffer with OMX buffer
       VERBOSE("Associating sebuffer %p with private %d\n", sebuffer, sebuffer->SEUserPrivate);
       buffer->sebuffer = sebuffer;
       buffer->bltBuffer = sebuffer;
       buffer->PTS = capture_buffer->native_presentation_time;

       buffer->eos = 0;
       list_add_tail(&buffer->list, &Context->filledBufferList);

       Context->numberOfCapturedFrame++;

       if(Context->thumbnail || omxse_display_disabled(Context->displayContext))
       {
           VERBOSE("Blit\n");
           omxse_blitter_performBliterring(buffer->bltBuffer->BlitterSurface, buffer->AppBlitterSurface,
                                           &buffer->bltBuffer->BlitterRect, &buffer->AppRect);
       }

       /*These values will be modified in omxse_blitter_resizeDisplayBufferInPlace function and
        * need to be restored when we have new capture buffer from streaming engine.
        */
       buffer->sebuffer->ResizedRect.size.w = buffer->bltBuffer->BlitterRect.size.w;
       buffer->sebuffer->ResizedRect.size.h = buffer->bltBuffer->BlitterRect.size.h;
       buffer->sebuffer->DisplayBuffer.src.visible_area.width = buffer->bltBuffer->BlitterRect.size.w;
       buffer->sebuffer->DisplayBuffer.src.visible_area.height = buffer->bltBuffer->BlitterRect.size.h;
       buffer->sebuffer->DisplayBuffer.src.Rect.width  = buffer->bltBuffer->BlitterRect.size.w;
       buffer->sebuffer->DisplayBuffer.src.Rect.height = buffer->bltBuffer->BlitterRect.size.h;

       // Since Step is just a state, not a counter, Ask step if buffer to fill
       needStep = (! list_empty(&Context->emptyBufferList));

       // Unlock before wake up to avoid ping/pong on mutex
       mutex_unlock(&Context->lock);

       wake_up_interruptible(&Context->grabDone);

       if(needStep)
           stm_se_play_stream_step(Context->Stream);
    }

FUNCTION_END:

    return Result;
}


//============================================================================
//  omxse_capture_open
//     call at module open
//============================================================================
static int omxse_capture_open(struct inode *inode, struct file *filp)
{
    struct omxse_context* Context;
    char name[32];
    int Result;

    Context = kzalloc(sizeof(struct omxse_context), GFP_KERNEL);
    if (!Context)
        return -ENOMEM;

    sprintf(Context->componentName, "%p", Context);
    DEBUG("OMXSE %s:: omxse_capture_open\n", Context->componentName);

    mutex_init(&Context->lock);
    mutex_init(&Context->lockStop);
    init_waitqueue_head(&Context->grabDone);
    init_waitqueue_head(&Context->injectionDone);
    init_waitqueue_head(&Context->emptyBuffersEvent);
    init_waitqueue_head(&Context->displayDone);

    INIT_LIST_HEAD(&Context->emptyBufferList);
    INIT_LIST_HEAD(&Context->filledBufferList);

    // Open Blitter
    Context->Blitter = stm_blitter_get(0);
    if (IS_ERR(Context->Blitter)) {
        Result = PTR_ERR(Context->Blitter);
        ERROR("OMXSE(): stm_blitter_get Failed (%d)\n", Result);
        goto alloc_error;
    }

    // Create new playback
    sprintf(name, "omx_playback%p", Context);
    Result = stm_se_playback_new(name, &Context->Playback);
    if (Result != 0) {
        ERROR("OMXSE(): stm_se_playback_new Failed (%d)\n", Result);
        goto alloc_error;
    }
    INFO("%s:: Playback created %p\n", Context->componentName, Context->Playback);

    sprintf(Context->StreamName, "bitstream%p", Context);
    sprintf(Context->SinkName, "source%p", Context);
    Context->Encoding = OMXSE_ENCODING_MPEG4P2;

    Context->state = STATE_OPEN;

    filp->private_data = (void*)Context;
    filp->f_pos = 0;

    return 0;

alloc_error:
    captureClose(Context);
    return Result;
}


//============================================================================
//  omxse_capture_release
//     call at module close: release the capture module
//============================================================================

static int omxse_capture_release(struct inode *inode, struct file *filp)
{
    struct omxse_context* Context = (struct omxse_context*)filp->private_data;

    mutex_lock(&Context->lock);

    captureStop(Context);

    mutex_unlock(&Context->lock);

    omxse_buffer_release(Context->componentName, Context);

    if(Context->Blitter)
    {
        stm_blitter_put(Context->Blitter);
        Context->Blitter = NULL;
    }

    captureClose(Context);

    return 0;
}

//============================================================================
//  setEncodingIoctl
//    management of OMXSE_SET_ENCODING IOCTL
//============================================================================
static long setEncodingIoctl(struct omxse_context* Context, void __user *argp)
{
        omxse_setencoding setencoding;

        if (copy_from_user(&setencoding, argp, sizeof(setencoding)))
            return -EFAULT;

        if(Context->state != STATE_OPEN)
            return -EINVAL;

        if(setencoding.Encoding >= OMXSE_ENCODING_MAX)
            return -EINVAL;
        mutex_lock(&Context->lock);

        Context->Encoding = Encodings[setencoding.Encoding];
        Context->state = STATE_CONFIGURED;
        Context->width = setencoding.width;
        Context->height = setencoding.height;
        Context->thumbnail = setencoding.thumbnail;
        Context->enable10Bit = setencoding.enable10Bit;
        strncpy(Context->componentName, setencoding.componentName, (OMXSE_NAME_LENGTH - 1));

        DEBUG("%s:: omxse_capture_ioctl (%p, OMXSE_SET_ENCODING, %d*%d, %d)\n",
                Context->componentName, Context,
                setencoding.width, setencoding.height, setencoding.enable10Bit);

        mutex_unlock(&Context->lock);

        return 0;
}

//============================================================================
//  startIoctl
//    management of OMXSE_START IOCTL
//============================================================================
static long startIoctl(struct omxse_context* Context)
{
        stm_object_h DisplayType;
        long Result = 0;
        uint32_t ctrlValue = STM_SE_CTRL_VALUE_VIDEO_DECODE_HD_PROFILE;

        if(Context->state != STATE_CONFIGURED)
            return -EINVAL;

        DEBUG("%s:: omxse_capture_ioctl (OMXSE_START)\n", Context->componentName);

        mutex_lock(&Context->lock);

        Result = stm_se_playback_set_speed(Context->Playback, 0);
        if (Result != 0) {
            ERROR("stm_se_playback_set_speed Failed\n");
            goto FUNCTION_ERROR;
        }

        ctrlValue = omxseGetMemProfile(Context);

        Result = stm_se_playback_set_control(Context->Playback, STM_SE_CTRL_VIDEO_DECODE_MEMORY_PROFILE, ctrlValue);
        if (Result != 0) {
            ERROR("stm_se_playback_set_control for 4k2k Failed\n");
            goto FUNCTION_ERROR;
        }

        Result = stm_se_play_stream_new(
                Context->StreamName,
                Context->Playback,
                Context->Encoding,
                &Context->Stream);
        if (Result != 0) {
            ERROR("stm_se_play_stream_new Failed\n");
            goto FUNCTION_ERROR;
        }

        Result = stm_registry_get_object(STM_REGISTRY_TYPES, "stm_se_video_grab", &DisplayType);
        if (Result != 0) {
            ERROR("stm_registry_get_object failed\n");
            goto FUNCTION_ERROR;
        }

        Context->Sink = (stm_object_h)&Context->Sink;
        Result = stm_registry_add_instance(STM_REGISTRY_INSTANCES, DisplayType, Context->SinkName, Context->Sink);
        if (Result != 0) {
            ERROR("stm_registry_add_instance failed\n");
            goto FUNCTION_ERROR;
        }

        Result = stm_se_play_stream_attach(Context->Stream, Context->Sink, STM_SE_PLAY_STREAM_OUTPUT_PORT_DEFAULT);
        if (Result != 0) {
            ERROR("stm_se_play_stream_attach failed\n");
            goto FUNCTION_ERROR;
        }

        // Connect callback
        stm_se_play_stream_register_buffer_capture_callback(
                Context->Stream,
                (stm_se_event_context_h)Context,
                (stream_buffer_capture_callback)captureCallback);

        // Reduce collated data for stream
        Result = stm_se_play_stream_set_control(
                Context->Stream,
                STM_SE_CTRL_REDUCE_COLLATED_DATA,
                STM_SE_CTRL_VALUE_APPLY);

        // Disable Sync as we will be using STEP
        Result = stm_se_play_stream_set_control(
                Context->Stream,
                STM_SE_CTRL_ENABLE_SYNC,
                STM_SE_CTRL_VALUE_DISAPPLY);

        // By default streaming engine is decimating the image by half
        // Need to overide this default policy
        Result = stm_se_play_stream_set_control(
                Context->Stream,
                STM_SE_CTRL_DECIMATE_DECODER_OUTPUT,
                STM_SE_CTRL_VALUE_DECIMATE_DECODER_OUTPUT_DISABLED);

        if (Result != 0) {
            ERROR("Unable to STM_SE_CTRL_DECIMATE_DECODER_OUTPUT\n");
            goto FUNCTION_ERROR;
        }
        Context->state = STATE_PLAY;

        omxse_display_flush(Context->displayContext,1);


        goto FUNCTION_END;

FUNCTION_ERROR:
        captureStop(Context);

FUNCTION_END:
        mutex_unlock(&Context->lock);
        return Result;
}

//============================================================================
//  stopIoctl
//    management of OMXSE_STOP IOCTL
//============================================================================
static long stopIoctl(struct omxse_context* Context)
{
        if(Context->state != STATE_PLAY)
            return -EINVAL;

        DEBUG("%s:: omxse_capture_ioctl (OMXSE_STOP)\n", Context->componentName);

        mutex_lock(&Context->lock);
        captureStop(Context);
        mutex_unlock(&Context->lock);
        return 0;

}

//============================================================================
//  useBufferIoctl
//    management of OMXSE_USE_BUFFER IOCTL
//============================================================================
static long useBufferIoctl(struct omxse_context* Context, void __user *argp)
{
        omxse_usebuffer usebuffer;
        stm_blitter_surface_address_t address;
        struct OMXBuffer *buffer;
        long Result = 0;

        if (copy_from_user(&usebuffer, argp, sizeof(usebuffer)))
            return -EFAULT;

        address.base = GetPhysicalContiguous(usebuffer.virtualAddr, usebuffer.bufferSize);
        if (!address.base)
            return -EIO;

        if(usebuffer.Color >= OMXSE_COLOR_MAX)
            return -EIO;

        mutex_lock(&Context->lock);

        buffer = (struct OMXBuffer *)kzalloc(sizeof(struct OMXBuffer), GFP_KERNEL);

        buffer->parentContext = Context;
        buffer->userID = usebuffer.userID;
        buffer->virtualAddr = usebuffer.virtualAddr;
        buffer->physicalAddr = address.base;
        buffer->bufferSize = usebuffer.bufferSize;
        buffer->AppRect.position.x = 0;
        buffer->AppRect.position.y = 0;
        buffer->AppRect.size.w = usebuffer.width;
        buffer->AppRect.size.h = usebuffer.height;

        if (colorBlitterCs[usebuffer.Color] != STM_BLITTER_SCS_RGB)
            address.cbcr_offset = usebuffer.width * colorStride[usebuffer.Color] * usebuffer.height;

        buffer->AppBlitterSurface = stm_blitter_surface_new_preallocated(
                colorBlitterFmt[usebuffer.Color], colorBlitterCs[usebuffer.Color],
                &address,
                usebuffer.bufferSize,
                &buffer->AppRect.size,
                usebuffer.width * colorStride[usebuffer.Color]);
        if (IS_ERR(buffer->AppBlitterSurface)) {
            Result = PTR_ERR(buffer->AppBlitterSurface);
            ERROR("stm_blitter_surface_new_preallocated failed for blitter output ! (%ld)\n", Result);
            ERROR("[parameters]\n");
            ERROR("|- fmt=%x\n", colorBlitterFmt[usebuffer.Color]);
            ERROR("|- cs=%x\n", colorBlitterCs[usebuffer.Color]);
            ERROR("|- addr.base=%lx\n", address.base);
            if (colorBlitterCs[usebuffer.Color] != STM_BLITTER_SCS_RGB)
                ERROR("|- addr.cbcr_offset=%ld\n", address.cbcr_offset);
            ERROR("|- size=%d\n", usebuffer.bufferSize);
            ERROR("|- width=%ld\n", buffer->AppRect.size.w);
            ERROR("|- height=%ld\n", buffer->AppRect.size.h);
            ERROR("|- stride=%d\n", usebuffer.width * colorStride[usebuffer.Color]);

            kfree(buffer);
            mutex_unlock(&Context->lock);
            return Result;
        }

        mutex_unlock(&Context->lock);

        omxse_buffer_registerOMX(Context->componentName, buffer);
        return 0;
}

//============================================================================
//  fillThisBufferIoctl
//    management of OMXSE_FILL_THIS_BUFFER IOCTL
//============================================================================
static long fillThisBufferIoctl(struct omxse_context* Context,unsigned long arg)
{
        struct OMXBuffer* buffer;
        int needStep;
        VERBOSE("OMXSE %s:: (fillThisBufferIoctl) (%lx)\n", Context->componentName, arg);

        mutex_lock(&Context->lock);

        buffer = omxse_buffer_findOMXByUserID(arg);
        if(buffer == NULL) {
            ERROR("%s:: OMXSE_FILL_THIS_BUFFER on an unregistered buffer (%lx)\n", Context->componentName, arg);
            mutex_unlock(&Context->lock);
            return -EINVAL;
        }

        // SE buffer still associated with OMX buffer ; release it to SE & undo association
        if(buffer->sebuffer != NULL)
        {
            VERBOSE("REL %p->%p\n", buffer, buffer->sebuffer);
            omxse_capture_ReleaseSEBuffer(buffer->sebuffer);
            buffer->sebuffer = NULL;
            Context->numberOfNotDisplayedFrame++;
        }

        needStep = list_empty(&Context->emptyBufferList);
        list_add_tail(&buffer->list, &Context->emptyBufferList);

        mutex_unlock(&Context->lock);

        // Ask decoding of one frame only when adding first empty frame
        if(needStep)
            stm_se_play_stream_step(Context->Stream);

        // Wake up potential waiting thread
        wake_up_interruptible(&Context->emptyBuffersEvent);
        return 0;
}

//============================================================================
//  fillBufferDoneIoctl
//    management of OMXSE_FILL_BUFFER_DONE IOCTL
//============================================================================
static long fillBufferDoneIoctl(struct omxse_context* Context, void __user *argp)
{
        omxse_grab grab;
        struct OMXBuffer* buffer;
        long Result = 0;

        VERBOSE("%s:: omxse_capture_ioctl (OMXSE_FILL_BUFFER_DONE)\n", Context->componentName);

        Result = wait_event_interruptible(Context->grabDone,
                (! list_empty(&Context->filledBufferList)) || Context->state == STATE_STOPPED);

        if(Result != 0)
            return Result;

        mutex_lock(&Context->lock);

        if(list_empty(&Context->filledBufferList) || Context->state != STATE_PLAY) {
            mutex_unlock(&Context->lock);
            return -EIO;
        }

        buffer = list_first_entry(&Context->filledBufferList, struct OMXBuffer, list);

        list_del(&buffer->list);

        mutex_unlock(&Context->lock);

        grab.userID = buffer->userID;
        grab.PTS = buffer->PTS;
        grab.eos = buffer->eos;

        if(copy_to_user(argp, &grab, sizeof(grab)))
            return -EFAULT;

        return 0;
}

//============================================================================
//  flushBufferIoctl
//    management of OMXSE_FLUSH_BUFFER IOCTL
//============================================================================
static long flushBufferIoctl(struct omxse_context* Context, void __user *argp)
{
        omxse_grab grab;
        struct OMXBuffer* buffer;

        mutex_lock(&Context->lock);

        if(! list_empty(&Context->emptyBufferList))
        {
            buffer = list_first_entry(&Context->emptyBufferList, struct OMXBuffer, list);
            list_del(&buffer->list);
            grab.userID = buffer->userID;
            VERBOSE("flushBufferIoctl flushing emptyBufferList : 0x%lx\n",  buffer->userID);
        }
        else
        {
            /* This part was added following fix for 31117 : moving the setting of stop state before
             * the callback has side effect that a new decode buffer could be available between setting
             * stop state and deactivating callback in captureStop() function
             * In that case the decode buffer need to be returned to upper layer (stagefright)
             */
            if(! list_empty(&Context->filledBufferList))
            {
                buffer = list_first_entry(&Context->filledBufferList, struct OMXBuffer, list);
                list_del(&buffer->list);
                grab.userID = buffer->userID;
                VERBOSE("flushBufferIoctl flushing  filledBufferList : 0x%lx\n",  buffer->userID);
            }
            else
            {
                grab.userID = 0;
            }
        }

        mutex_unlock(&Context->lock);

        if(copy_to_user(argp, &grab, sizeof(grab)))
            return -EFAULT;

        return 0;
}

//============================================================================
//  injectEosIoctl
//    management of OMXSE_INJECT_EOS IOCTL
//============================================================================
static long injectEosIoctl(struct omxse_context* Context)
{
        if(Context->state != STATE_PLAY)
            return -EINVAL;

 	stm_se_play_stream_inject_discontinuity(Context->Stream, 0, 0, 1);
        return 0;
}

//============================================================================
//  injectDiscontinuityIoctl
//    management of OMXSE_INJECT_DISCONTINUITY IOCTL
//============================================================================
static long injectDisclontinuityIoctl(struct omxse_context* Context)
{
        long Result = 0;
        DEBUG("%s:: omxse_capture_ioctl (OMXSE_INJECT_DISCONTINUITY)\n", Context->componentName);

        if(Context->state != STATE_PLAY)
            return -EINVAL;

        Result = stm_se_play_stream_drain(Context->Stream, 1);
        if (Result != 0) {
            ERROR("stm_se_play_stream_drain Failed (%ld)\n", Result);
            return Result;
        }

        // Flush Display in order to give back buffer to SE
        // No reason to remove frames on display
        omxse_display_flush(Context->displayContext, 0);

        Result = stm_se_play_stream_inject_discontinuity(Context->Stream, 0, 1, false);

        if (Result != 0) {
            ERROR("stm_se_play_stream_inject_discontinuity Failed (%ld)\n", Result);
            return Result;
        }

        return 0;
}

//============================================================================
//  omxse_capture_ioctl
//     management of IOCTLs capture module
//============================================================================
static long omxse_capture_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    struct omxse_context* Context = (struct omxse_context*)filp->private_data;
    void __user *argp = (void __user *) arg;
    long Result = 0;

    switch(cmd)
    {
        case OMXSE_SET_ENCODING: {
            Result = setEncodingIoctl(Context, argp);
        }; break;

        case OMXSE_START: {
            Result = startIoctl(Context);
        }; break;

        case OMXSE_STOP: {
            Result = stopIoctl(Context);
        }; break;

        case OMXSE_USE_BUFFER: {
            Result = useBufferIoctl(Context, argp);
        }; break;

        case OMXSE_FILL_THIS_BUFFER: {
            Result = fillThisBufferIoctl(Context, arg);
        }; break;

        case OMXSE_FILL_BUFFER_DONE: {
           Result = fillBufferDoneIoctl(Context, argp);
        }; break;

        case OMXSE_FLUSH_BUFFER: {
           Result = flushBufferIoctl(Context, argp);
        }; break;

        case OMXSE_INJECT_EOS: {
           Result = injectEosIoctl(Context);
        }; break;

        case OMXSE_INJECT_DISCONTINUITY: {
           Result = injectDisclontinuityIoctl(Context);
        }; break;

        default:
            Result =  -EINVAL;
    }

    return Result;
}

//============================================================================
//  omxse_capture_write
//     call at module write: new buffer to decode
//============================================================================
static ssize_t omxse_capture_write(struct file *filp,
        const char __user * Buffer,
        size_t Count,
        loff_t *f_pos)
{
    struct omxse_context* Context = (struct omxse_context*)filp->private_data;
    int Result;

    //VERBOSE("%s:: omxse_capture_write (%d)\n", Context->componentName, Count);

    if(Context->state != STATE_PLAY)
        return -EINVAL;

    if(Count > Context->kernBufferSize)
    {
        if(Context->kernBuffer)
            kfree(Context->kernBuffer);
        Context->kernBuffer = (char*) kmalloc(Count, GFP_KERNEL);
        if(Context->kernBuffer == NULL) {
           ERROR("%s:: new buffer alloc failed\n", __func__);
           return -ENOMEM;
        }
        Context->kernBufferSize = Count;
    }

    Context->injectionPending = true;

    if (copy_from_user(Context->kernBuffer, Buffer, Count)) {
        ERROR("%s:: copy from user failed\n", __func__);
        Result = -EFAULT;
    }
    else
    {
        /* We can always inject from Kernel, copy_from_user not necessary, however we should do an access_ok check above this */
        /* Lock not required, since inject_data & discontinuity already protected */
        Result = stm_se_play_stream_inject_data(Context->Stream, Context->kernBuffer, Count);

        Context->injectionPending = false;
        /* In case stop request occured while injecting was waiting for available buffer we need to unblock stop process */
        if (Context->state == STATE_STOPPED)
        {
            INFO("%s:: omxse_capture_write: Stop command occurred during injection. Send signal injectionDone\n", Context->componentName);
            wake_up_interruptible(&Context->injectionDone);
        }
    }

    return Result;
}


static struct cdev cdev;
static dev_t omxse_capture_devno;


//============================================================================
//  file_operations
//     list of capture module entry points
//============================================================================

static struct file_operations fops =
{
    .owner          = THIS_MODULE,
    .open           = omxse_capture_open,
    .release        = omxse_capture_release,
    .unlocked_ioctl = omxse_capture_ioctl,
    .write          = omxse_capture_write,
};


//============================================================================
//  omxse_capture_init
//     called when capture module is loaded
//============================================================================
int omxse_capture_init(dev_t devno, struct class *omxse_class, const char* name)
{
    int err;
    struct device * mdev;

    cdev_init(&cdev, &fops);
    err = cdev_add(&cdev, devno, 1);
    if (0 != err) {
        ERROR("cdev_add\n");
        return err;
    }

    mdev = device_create(omxse_class, NULL, devno, NULL, name, MINOR(devno));
    if (IS_ERR(mdev)) {
        ERROR("device_create\n");
        cdev_del(&cdev);
        return PTR_ERR(mdev);
    }

    defaultFrameDisplay = kzalloc(sizeof(struct SEBuffer), GFP_KERNEL);

    if (defaultFrameDisplay == NULL) {
        ERROR("Out of memory for allocating display supplementary SE Buffer\n");
        err =  1;
        return err;
    }
    defaultFrameVirtAddr = NULL;

    omxse_capture_devno = devno;

    return 0;
}


//============================================================================
//  omxse_capture_done
//     called when capture module is unloaded
//============================================================================

void omxse_capture_done(struct class *omxse_class)
{
    unsigned long defaultFramePhysAddr = defaultFrameDisplay->DisplayBuffer.src.primary_picture.video_buffer_addr;

    if(defaultFramePhysAddr)
    {
        if(free_display_buffer((void *)defaultFramePhysAddr, DISPLAY_BUF_BPA2_PARTITION) < 0) {
            printk(KERN_ERR"%s: Failed to free default Display buffer \n", __FUNCTION__);
        }
    }

    if (defaultFrameDisplay) {
        kfree(defaultFrameDisplay);
        defaultFrameDisplay = NULL;
    }

    device_destroy(omxse_class, omxse_capture_devno);
    cdev_del(&cdev);
}
