/************************************************************************
Copyright (C) 2005 STMicroelectronics. All Rights Reserved.

This file is part of the Dvb_adaptation Library.

Dvb_adaptation is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Dvb_adaptation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Dvb_adaptation Library may alternatively be licensed under a proprietary
license from ST.

Source file name : omx_module.c
Author :           Jean-Philippe Fassino

************************************************************************/

#include <linux/module.h>            /* kernel module definitions */
#include <linux/fs.h>                /* file system operations */
#include <linux/cdev.h>              /* character device definitions */
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/wait.h>
#include <linux/sched.h>

#include "stm_se.h"
#include <linux/stm/blitter.h>
#include <include/stmdisplay.h>

#include "omxse.h"
#include "omxse_internal.h"

MODULE_DESCRIPTION
    ("Linux driver for STM OMX IL implementation.");
MODULE_AUTHOR("Jean-Philippe FASSINO");
MODULE_LICENSE("GPL");

#define STM_DISPLAY_DEVICE  0

static struct class *omxse_class;
static dev_t omxse_dev = 0;


//============================================================================
//  StmLoadModule
//  called when module is loaded
//============================================================================
static int /*__init*/ StmLoadModule(void)
{
    dev_t dev = 0;
    int err;

    err = alloc_chrdev_region(&dev, 0, 3, "streamer");
    if (0 != err)
    {
        ERROR("alloc_chrdev_region\n");
        goto out_error;
    }

    omxse_class = class_create(THIS_MODULE, "streamer");
    if (IS_ERR(omxse_class))
    {
        ERROR("class_create\n");
        err = PTR_ERR(omxse_class);
        goto out_region;
    }

    err = omxse_capture_init(MKDEV(MAJOR(dev), 0), omxse_class, "omxse.grabber");
    if (0 != err)
        goto out_class;

    err = omxse_display_init(MKDEV(MAJOR(dev), 1), omxse_class, "omxse.comp");
    if (0 != err)
        goto out_grabber;

    err = omxse_blitter_init(MKDEV(MAJOR(dev), 2), omxse_class, "omxse.blitter");
    if (0 != err)
        goto out_comp;

    err = omxse_vsync_init(MKDEV(MAJOR(dev), 3), omxse_class, "omxse.vsync");
    if (0 != err)
        goto out_vsync;

    INFO("%s loaded Major=%d\n", MODULE_NAME, MAJOR(dev));

    omxse_buffer_init();

    omxse_dev = dev;

    return 0;

out_vsync:
    omxse_blitter_done(omxse_class);
out_comp:
    omxse_display_done(omxse_class);
out_grabber:
    omxse_capture_done(omxse_class);
out_class:
    class_destroy(omxse_class);
out_region:
    unregister_chrdev_region(dev, 3);
out_error:
    return err;
}


//============================================================================
//  StmUnloadModule
//  called when module is unloaded
//============================================================================
static void /*__exit*/ StmUnloadModule(void)
{
    INFO("UnLoad %s\n", MODULE_NAME);

    omxse_vsync_done(omxse_class);
    omxse_blitter_done(omxse_class);
    omxse_display_done(omxse_class);
    omxse_capture_done(omxse_class);

    class_destroy(omxse_class);

    unregister_chrdev_region(omxse_dev, 3);
}

module_init(StmLoadModule);
module_exit(StmUnloadModule);

