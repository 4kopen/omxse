/************************************************************************
Copyright (C) 2005 STMicroelectronics. All Rights Reserved.

This file is part of the Dvb_adaptation Library.

Dvb_adaptation is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Dvb_adaptation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with player2; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Dvb_adaptation Library may alternatively be licensed under a proprietary
license from ST.

Source file name : omxse_display.c
Author :           Jean-Philippe Fassino

************************************************************************/
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/highmem.h>

#include "omxse.h"
#include "omxse_internal.h"


struct omxse_buffers {
    spinlock_t                      wlock;
    struct mutex                    lock;

    struct list_head                OMXBufferList;
    struct list_head                SEBufferList;
    struct list_head                WorkBufferList;
    struct work_struct              work;
};

struct omxse_buffers Buffers;

//============================================================================
//  omxse_buffer_registerOMX
//============================================================================
void omxse_buffer_registerOMX(const char* componentName, struct OMXBuffer* buffer)
{
    DEBUG("%s:: New OMXBuffer(%p<%lx>, @=%lx, %ldx%ld=%ld)\n",
            componentName, buffer, buffer->userID, buffer->physicalAddr,
            buffer->AppRect.size.w, buffer->AppRect.size.h, buffer->bufferSize);

    mutex_lock(&Buffers.lock);

    list_add_tail(&buffer->alllist, &Buffers.OMXBufferList);

    mutex_unlock(&Buffers.lock);
}

//============================================================================
//  omxse_buffer_registerSE
//============================================================================
void omxse_buffer_registerSE(const char* componentName, struct SEBuffer* sebuffer)
{
    DEBUG("%s:: New SEBuffer(%p @=%lx)\n",
            componentName, sebuffer, sebuffer->BlitterAddr.base);

    mutex_lock(&Buffers.lock);

    list_add_tail(&sebuffer->alllist, &Buffers.SEBufferList);

    mutex_unlock(&Buffers.lock);
}

//============================================================================
//  omxse_buffer_release
//============================================================================
void omxse_buffer_release(const char* componentName, void* parentContext)
{
    struct OMXBuffer *buffer, *tmpBuffer;
    struct SEBuffer *sebuffer, *setmpBuffer;
    cancel_work_sync(&Buffers.work);
    mutex_lock(&Buffers.lock);

    list_for_each_entry_safe(buffer, tmpBuffer, &Buffers.OMXBufferList, alllist)
    {
        if(buffer->parentContext == parentContext)
        {
            DEBUG("%s:: Delete OMXBuffer(%p<%lx>, @=%lx)\n",
                    componentName, buffer, buffer->userID, buffer->physicalAddr);

            stm_blitter_surface_put(buffer->AppBlitterSurface);
            list_del(&buffer->alllist);
            kfree(buffer);
        }
    }

    list_for_each_entry_safe(sebuffer, setmpBuffer, &Buffers.SEBufferList, alllist)
    {
        if(sebuffer->parentContext == parentContext)
        {
            DEBUG("%s:: Delete SEBuffer(%p @=%lx)\n",
                    componentName, sebuffer, sebuffer->BlitterAddr.base);

            stm_blitter_surface_put(sebuffer->BlitterSurface);
            list_del(&sebuffer->alllist);
            kfree(sebuffer);
        }
    }
    mutex_unlock(&Buffers.lock);

}

//============================================================================
//  omxse_buffer_dissociate
//============================================================================
void omxse_buffer_dissociate(const char* componentName, void* parentContext)
{
    struct OMXBuffer *buffer, *tmpBuffer;

    mutex_lock(&Buffers.lock);

    list_for_each_entry_safe(buffer, tmpBuffer, &Buffers.OMXBufferList, alllist)
    {
        if(buffer->parentContext == parentContext)
        {
            DEBUG("%s:: Dissociate OMXBuffer(%p<%lx>, @=%lx)\n",
                    componentName, buffer, buffer->userID, buffer->physicalAddr);

            if (buffer->bltBuffer) {
              omxse_blitter_performBliterring(buffer->bltBuffer->BlitterSurface, buffer->AppBlitterSurface,
                                              &buffer->bltBuffer->ResizedRect, &buffer->AppRect);
            }
            buffer->sebuffer = buffer->bltBuffer = NULL;
        }
    }

    mutex_unlock(&Buffers.lock);

}


//============================================================================
//  omxse_buffer_findOMXByUserID
//============================================================================
struct OMXBuffer* omxse_buffer_findOMXByUserID(unsigned long userId)
{
    struct OMXBuffer *buffer;

    mutex_lock(&Buffers.lock);

    list_for_each_entry(buffer, &Buffers.OMXBufferList, alllist)
    {
        if(buffer->userID == userId) {
            mutex_unlock(&Buffers.lock);
            return buffer;
        }
    }

    mutex_unlock(&Buffers.lock);

    return NULL;
}


//============================================================================
//  omxse_buffer_findOMXByPhysAddr
//============================================================================
struct OMXBuffer* omxse_buffer_findOMXByPhysAddr(unsigned long physicalAddr)
{
    struct OMXBuffer *buffer;

    mutex_lock(&Buffers.lock);

    list_for_each_entry(buffer, &Buffers.OMXBufferList, alllist)
    {
        if(buffer->physicalAddr == physicalAddr) {
            mutex_unlock(&Buffers.lock);
            return buffer;
        }
    }

    mutex_unlock(&Buffers.lock);
    return NULL;
}

//============================================================================
//  omxse_buffer_findSE
//============================================================================
struct SEBuffer* omxse_buffer_findSE(unsigned long physicalAddr)
{
    struct SEBuffer *sebuffer;

    mutex_lock(&Buffers.lock);

    list_for_each_entry(sebuffer, &Buffers.SEBufferList, alllist)
    {
        if(sebuffer->BlitterAddr.base == physicalAddr) {
            mutex_unlock(&Buffers.lock);
            return sebuffer;
        }
    }

    mutex_unlock(&Buffers.lock);
    return NULL;
}

//============================================================================
//  omxse_buffer_stillALive
//============================================================================
struct SEBuffer* omxse_buffer_stillALive(struct SEBuffer* sebuffer)
{
    struct SEBuffer *dummy_buffer;

    mutex_lock(&Buffers.lock);

    list_for_each_entry(dummy_buffer, &Buffers.SEBufferList, alllist)
    {
        if(sebuffer == dummy_buffer) {
            mutex_unlock(&Buffers.lock);
            return sebuffer;
        }
    }

    mutex_unlock(&Buffers.lock);
    return NULL;
}

void omxse_release_sebuffer(struct SEBuffer *sebuffer)
{
    if(sebuffer->SEUserPrivate != 0xFFFFFFFF)
    {
        sebuffer->SEReleaseBuffer(sebuffer->SEReleaseBufferContext,sebuffer->SEUserPrivate);
        sebuffer->SEUserPrivate = 0xFFFFFFFF;
    }
}

void omxse_release_sebuffers(void* parentContext)
{
    struct SEBuffer *sebuffer;
    mutex_lock(&Buffers.lock);
    VERBOSE("OMXSE Releasing Buffers of Context=%p\n", parentContext);

    list_for_each_entry(sebuffer, &Buffers.SEBufferList, alllist)
    {
        if(sebuffer->parentContext == parentContext)
            omxse_release_sebuffer(sebuffer);
    }
    mutex_unlock(&Buffers.lock);
}


void omxse_buffer_delayedRelease(struct SEBuffer* sebuffer)
{
    u_long flags;
    /* TODO Current work around solution is to check before queueing in WorkBufferList
    if the sebuffer exists in sebufferlist. There maybe issues if the sebuffer is freed
    and another one is allocated at the same address in another OMXSE instance */
    if (omxse_buffer_stillALive(sebuffer) == NULL)
        return;

    spin_lock_irqsave(&Buffers.wlock, flags);
    /* add buffer to work list to process */
    list_add_tail(&sebuffer->worklist, &Buffers.WorkBufferList);
    spin_unlock_irqrestore(&Buffers.wlock, flags);

    schedule_work(&Buffers.work);
}

static void omxse_buffer_work(struct work_struct *work)
{
    struct omxse_buffers *buffers = container_of(work, struct omxse_buffers, work);
    struct SEBuffer *sebuffer;
    u_long flags;
    int empty;

    spin_lock_irqsave(&buffers->wlock, flags);
    empty = list_empty(&buffers->WorkBufferList);
    spin_unlock_irqrestore(&buffers->wlock, flags);

    while (!empty) {
        spin_lock_irqsave(&buffers->wlock, flags);
        sebuffer = list_first_entry(&buffers->WorkBufferList, struct SEBuffer, worklist);
        list_del(&sebuffer->worklist);
        spin_unlock_irqrestore(&buffers->wlock, flags);

        if (omxse_buffer_stillALive(sebuffer)) {
        mutex_lock(sebuffer->parentLockStop);
        if (*sebuffer->parentState == STATE_PLAY) {
               omxse_release_sebuffer(sebuffer);
            }
          mutex_unlock(sebuffer->parentLockStop);
        }
        spin_lock_irqsave(&buffers->wlock, flags);
        empty = list_empty(&buffers->WorkBufferList);
        spin_unlock_irqrestore(&buffers->wlock, flags);
    }
}

//============================================================================
//  omxse_buffer_init
//============================================================================
void omxse_buffer_init()
{
    spin_lock_init(&Buffers.wlock);
    mutex_init(&Buffers.lock);

    INIT_LIST_HEAD(&Buffers.OMXBufferList);
    INIT_LIST_HEAD(&Buffers.SEBufferList);
    INIT_LIST_HEAD(&Buffers.WorkBufferList);
    INIT_WORK(&Buffers.work, omxse_buffer_work);
}

//============================================================================
//  GetPhysicalContiguous
//============================================================================
unsigned long GetPhysicalContiguous(unsigned long ptr, size_t size)
{
    struct mm_struct *mm = current->mm;
    //struct vma_area_struct *vma = find_vma(mm, ptr);
    unsigned virt_base = (ptr / PAGE_SIZE) * PAGE_SIZE;
    unsigned phys_base = 0;

    pgd_t *pgd;
    pmd_t *pmd;
    pte_t *ptep, pte;

    u_long flags;

    spin_lock_irqsave(&mm->page_table_lock,flags);

    pgd = pgd_offset(mm, virt_base);
    if (pgd_none(*pgd) || pgd_bad(*pgd))
        goto out;

    pmd = pmd_offset((pud_t *) pgd, virt_base);
    if (pmd_none(*pmd) || pmd_bad(*pmd))
        goto out;

    ptep = pte_offset_map(pmd, virt_base);

    if (!ptep)
        goto out;

    pte = *ptep;

    if (pte_present(pte)) {
        phys_base = __pa(page_address(pte_page(pte)));
    }

    if (!phys_base)
        goto out;

    spin_unlock_irqrestore(&mm->page_table_lock,flags);
    return phys_base + (ptr - virt_base);

out:
    spin_unlock_irqrestore(&mm->page_table_lock,flags);
    return 0;
}
